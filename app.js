/* istanbul ignore file */
const express = require('express');
const fs = require('fs');
const helmet = require('helmet');
const morgan = require('morgan');
const config = require('config');
const cors = require('cors');
const corsOptions = {
  origin: ['http://localhost:4200']
}

const MESS = require('./config/messages');
const response = require('./helpers/response');

const authRtr = require('./routes/auth');
const userRtr = require('./routes/user');
const myRtr = require('./routes/my');
const logRtr = require('./routes/log');
const UserAuthMdw = require('./middlewares/user-auth');

const app = express();
app.set('env', process.env.NODE_ENV || 'development');

app.use(helmet());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors(corsOptions));

if (config.log.FILE === true) {
  try {
    if (!fs.existsSync('./bin/logs')) {
      fs.mkdirSync('./bin/logs');
    }
    if (!fs.existsSync('./bin/logs/' + app.get('env'))) {
      fs.mkdirSync('./bin/logs/' + app.get('env'));
    }
  }
  catch (err) {
    res.status(MESS.SYSTEM.ERRORS.LOG_INSUFFISANT_RIGHTS.CODE_HTTP).json({
      message: 'Unable to write logs',
      error: {
        message: MESS.SYSTEM.ERRORS.LOG_INSUFFISANT_RIGHTS.MESSAGE
      }
    });
  }
  const accessLogStream = fs.createWriteStream('./bin/logs/' + app.get('env') + '/access.log', { flags: 'a' });
  const errorLogStream = fs.createWriteStream('./bin/logs/' + app.get('env') + '/error.log', { flags: 'a' });
  app.use(morgan(config.log.FILE_FORMAT, {
    stream: errorLogStream, skip: (req, res) => {
      return res.statusCode < 400;
    }
  }));
  app.use(morgan(config.log.FILE_FORMAT, {
    stream: accessLogStream, skip: function (req, res) {
      return res.statusCode >= 400;
    }
  }));
}
if (config.log.CONSOLE === true) {
  app.use(morgan('dev'));
}

app.use((req, res, next) => {
  try {
    const Sequelize = require('sequelize');
    const sequelize = new Sequelize(config.db.NAME, config.db.USER, config.db.PASSWORD, {
      host: config.db.URL,
      port: config.db.PORT,
      dialect: 'mysql',
      logging: false
    });
    sequelize.authenticate();
    next();
  } catch (err) {
    res.status(MESS.SYSTEM.DB.ERRORS.CONNEXION.CODE_HTTP).json({
      message: MESS.SYSTEM.DB.ERRORS.CONNEXION.MESSAGE,
      error: {
        message: 'failed to connect to server'
      }
    });
  }
});

app.use((req, res, next) => {
  req.locals = { user: {}, profile: {}, customquery: {} };
  res.locals = { customquery: {}, links: [], notices: [] };
  next();
});

app.use('/api/' + config.app.API_VERSION + '/auth', authRtr);
app.use('/api/' + config.app.API_VERSION + '/my', myRtr);
app.use('/api/' + config.app.API_VERSION + '/users', userRtr);
app.use('/api/' + config.app.API_VERSION + '/logs', [UserAuthMdw.isAuth, UserAuthMdw.isAdmin], logRtr);


if (app.get('env') === 'development') {
  const swaggerJSDoc = require('swagger-jsdoc');
  const swaggerSpec = swaggerJSDoc(require('./config/swagger/schema'));
  app.get('/swagger.json', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
  });
}

app.use((req, res, next) => {
  req.locals.message = MESS.SYSTEM.ERRORS.PAGE_NOT_FOUND;
  response.error(req, res, next, MESS.SYSTEM.ERRORS.PAGE_NOT_FOUND);
});

module.exports = app;
