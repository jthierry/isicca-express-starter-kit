process.env.NODE_ENV = 'testing';
const config = require('config');
const LANG = config.app.LANG.toUpperCase();

const User = require('../models/').User;
const Log = require('../models/').Log;

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const expect = chai.expect;

const MESS = require('../config/messages');

chai.use(chaiHttp);

const baseUrl = '/api/' + config.app.API_VERSION;

const _ADMIN = {
    email: 'admin@test.com',
    password: 'P@ssw0rd',
    firstName: 'prénom',
    lastName: 'nom',
    role: 'admin'
};
const _USER = {
    email: 'user@test.com',
    password: 'P@ssw0rd',
    firstName: 'prénom',
    lastName: 'nom',
    role: 'user'
};
const _USER2 = {
    email: 'user2@test.com',
    password: 'P@ssw0rd2',
    firstName: 'prénom2',
    lastName: 'nom2',
    role: 'user'
};

const INVALID_PASSWORD = 'P@ssw0d';
const INVALID_ID = '111111'
const UNKNOWN_ID = '11111111-1111-1111-1111-111111111111';
let LOG_ID;
let USER_ID;
let USER_TOKEN;
let USER_LOGID;
let USER2_ID;
let USER2_TOKEN;
let USER2_LOGID;
let ADMIN_ID;
let ADMIN_TOKEN;
let ADMIN_LOGID;


describe('Admin commands', () => {
    before(async () => {
        await Log.destroy({
            where: {},
        }, err => { });
        await User.destroy({
            where: {},
        }, err => { });
    });

    /*
    * Preparing admin & users
    */
    describe('/POST register & login', () => {
        it('[' + MESS.USER.REGISTER.SUCCESS.CODE_HTTP + '] it should register my first user ', done => {
            const user = { ..._USER };
            chai
                .request(server)
                .post(baseUrl + '/auth/register')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.REGISTER.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad Response').to.be.empty;
                    done();
                });
        });
        it('[' + MESS.USER.LOGIN.SUCCESS.CODE_HTTP + '] it should login my first user', done => {
            const user = { ..._USER };
            chai
                .request(server)
                .post(baseUrl + '/auth/login')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.LOGIN.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(1);
                    expect(res.body.results[0], 'Bad result').to.be.a('object');
                    expect(res.body.results[0], 'Bad result token').to.have.property('token').to.be.a('string');
                    expect(res.body.results[0], 'Bad result expires').to.have.property('expiresIn').to.be.a('number').to.equal(config.user.JWT_EXPIRATIONTIME_HOUR * 60 * 60);
                    expect(res.body.results[0], 'Bad result profile').to.have.property('profile').to.be.a('object');
                    expect(res.body.results[0].profile, 'Bad result profile email').to.have.property('email').to.be.a('string').to.equal(_USER.email);
                    expect(res.body.results[0].profile, 'Bad result profile firstName').to.have.property('firstName').to.be.a('string').to.equal(_USER.firstName);
                    expect(res.body.results[0].profile, 'Bad result profile lastName').to.have.property('lastName').to.be.a('string').to.equal(_USER.lastName);
                    expect(res.body.results[0].profile, 'Bad result profile id').to.have.property('id').to.be.a('string');
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    USER_TOKEN = res.body.results[0].token;
                    USER_ID = res.body.results[0].profile.id;
                    done();
                });
        });
        it('[' + MESS.USER.REGISTER.SUCCESS.CODE_HTTP + '] it should register my admin', done => {
            const user = { ..._ADMIN };
            chai
                .request(server)
                .post(baseUrl + '/auth/register')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.REGISTER.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad Response').to.be.empty;
                    done();
                });
        });
        it('[' + MESS.USER.LOGIN.SUCCESS.CODE_HTTP + '] it should login my admin', done => {
            const user = { ..._ADMIN };
            chai
                .request(server)
                .post(baseUrl + '/auth/login')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.LOGIN.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(1);
                    expect(res.body.results[0], 'Bad result').to.be.a('object');
                    expect(res.body.results[0], 'Bad result token').to.have.property('token').to.be.a('string');
                    expect(res.body.results[0], 'Bad result expires').to.have.property('expiresIn').to.be.a('number').to.equal(config.user.JWT_EXPIRATIONTIME_HOUR * 60 * 60);
                    expect(res.body.results[0], 'Bad result profile').to.have.property('profile').to.be.a('object');
                    expect(res.body.results[0].profile, 'Bad result profile email').to.have.property('email').to.be.a('string').to.equal(_ADMIN.email);
                    expect(res.body.results[0].profile, 'Bad result profile firstName').to.have.property('firstName').to.be.a('string').to.equal(_ADMIN.firstName);
                    expect(res.body.results[0].profile, 'Bad result profile lastName').to.have.property('lastName').to.be.a('string').to.equal(_ADMIN.lastName);
                    expect(res.body.results[0].profile, 'Bad result profile id').to.have.property('id').to.be.a('string');
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    ADMIN_TOKEN = res.body.results[0].token;
                    ADMIN_ID = res.body.results[0].profile.id;
                    done();
                });
        });
    });

    describe('/POST users', () => {
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not register an user (auth token missing)', done => {
            const user = { ..._USER2 };
            chai
                .request(server)
                .post(baseUrl + '/users/')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.REGISTER.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not register an user (bad auth token)', done => {
            const user = { ..._USER2 };
            const token = 'Bearer: 1' + ADMIN_TOKEN;
            chai
                .request(server)
                .post(baseUrl + '/users/')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.REGISTER.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.MISSING_EMAIL.CODE_HTTP + '] it should not register an user (missing email)', done => {
            const user = { ..._USER2 };
            delete user.email;
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .post(baseUrl + '/users/')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_EMAIL.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.REGISTER.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_EMAIL.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_EMAIL.CODE_HTTP + '] it should not register an user (invalid email)', done => {
            const user = { ..._USER2 };
            user.email += '1';
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .post(baseUrl + '/users/')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_EMAIL.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.REGISTER.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_EMAIL.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.MISSING_PASSWORD.CODE_HTTP + '] it should not register an user (missing password)', done => {
            const user = { ..._USER2 };
            delete user.password;
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .post(baseUrl + '/users/')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_PASSWORD.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.REGISTER.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_PASSWORD.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_PASSWORD.CODE_HTTP + '] it should not register an user (password too short)', done => {
            const user = { ..._USER2 };
            user.password = INVALID_PASSWORD;
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .post(baseUrl + '/users/')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_PASSWORD.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.REGISTER.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_PASSWORD.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.REGISTER.SUCCESS.CODE_HTTP + '] it should register user ', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            const user = { ..._USER2 };
            chai
                .request(server)
                .post(baseUrl + '/users/')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.REGISTER.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad Response').to.be.empty;
                    done();
                });
        });
    });

    describe('/GET users', () => {
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not register an user (auth token missing)', done => {
            chai
                .request(server)
                .get(baseUrl + '/users/')
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.ADMIN.FETCH_ALL.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not fetch all users (bad auth token)', done => {
            const token = 'Bearer: 1' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/users/')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.ADMIN.FETCH_ALL.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.NOT_ADMIN.CODE_HTTP + '] it should not fetch all users (user auth token)', done => {
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/users/')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.NOT_ADMIN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.ADMIN.FETCH_ALL.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.NOT_ADMIN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.ADMIN.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should fetch all users ', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/users/')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.ADMIN.FETCH_ALL.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.ADMIN.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(3);
                    expect(res.body.results[0], 'Bad result').to.be.a('object');
                    expect(res.body.results[0], 'Bad result email').to.have.property('email').to.be.a('string');
                    expect(res.body.results[0], 'Bad result firstName').to.have.property('firstName').to.be.a('string');
                    expect(res.body.results[0], 'Bad result lastName').to.have.property('lastName').to.be.a('string');
                    expect(res.body.results[0], 'Bad result id').to.have.property('id').to.be.a('string');
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
    });

    describe('/GET user', () => {
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not return an user (auth token missing)', done => {
            chai
                .request(server)
                .get(baseUrl + '/users/' + USER_ID)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.FETCH_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not return an user (bad auth token)', done => {
            const token = 'Bearer: 1' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/users/' + USER_ID)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.FETCH_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.NOT_ADMIN.CODE_HTTP + '] it should not return an user (user auth token)', done => {
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/users/' + USER_ID)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.NOT_ADMIN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.FETCH_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.NOT_ADMIN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.SYSTEM.DB.ERRORS.INVALID_ID.CODE_HTTP + '] it should not return an user (invalid format user id)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/users/' + INVALID_ID)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.SYSTEM.DB.ERRORS.INVALID_ID.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.FETCH_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.SYSTEM.DB.ERRORS.INVALID_ID.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_ID.CODE_HTTP + '] it should not return an user (unknown user id)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/users/' + UNKNOWN_ID)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_ID.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.FETCH_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_ID.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.FETCH_PROFILE.SUCCESS.CODE_HTTP + '] it should return an user', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/users/' + USER_ID)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.FETCH_PROFILE.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.FETCH_PROFILE.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(1);
                    expect(res.body.results[0], 'Bad result').to.be.a('object');
                    expect(res.body.results[0], 'Bad result email').to.have.property('email').to.be.a('string').to.equal(_USER.email);
                    expect(res.body.results[0], 'Bad result firstName').to.have.property('firstName').to.be.a('string').to.equal(_USER.firstName);
                    expect(res.body.results[0], 'Bad result lastName').to.have.property('lastName').to.be.a('string').to.equal(_USER.lastName);
                    expect(res.body.results[0], 'Bad result id').to.have.property('id').to.be.a('string');
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
    });

    describe('/PUT update user profile', () => {
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not update the profile of an user (auth token missing)', done => {
            const user = {
                profile: { lastName: _USER2.lastName }
            };
            chai
                .request(server)
                .put(baseUrl + '/users/' + USER_ID)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not update the profile of an user (bad auth token)', done => {
            const token = 'Bearer: 1' + ADMIN_TOKEN;
            const user = {
                profile: { lastName: _USER2.lastName }
            };
            chai
                .request(server)
                .put(baseUrl + '/users/' + USER_ID)
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.NOT_ADMIN.CODE_HTTP + '] it should not update the profile of an user (user auth token)', done => {
            const token = 'Bearer: ' + USER_TOKEN;
            const user = {
                profile: { lastName: _USER2.lastName }
            };
            chai
                .request(server)
                .put(baseUrl + '/users/' + USER_ID)
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.NOT_ADMIN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.NOT_ADMIN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.SYSTEM.DB.ERRORS.INVALID_ID.CODE_HTTP + '] it should not update the profile of an user (invalid format user id)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            const user = {
                profile: { lastName: _USER2.lastName }
            };
            chai
                .request(server)
                .put(baseUrl + '/users/' + INVALID_ID)
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.SYSTEM.DB.ERRORS.INVALID_ID.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.SYSTEM.DB.ERRORS.INVALID_ID.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_ID.CODE_HTTP + '] it should not update the profile of an user (unknown user id)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            const user = {
                profile: { lastName: _USER2.lastName }
            };
            chai
                .request(server)
                .put(baseUrl + '/users/' + UNKNOWN_ID)
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_ID.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_ID.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.MISSING_PROFILE.CODE_HTTP + '] it should not update the profile of an user (profile missing)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            const user = {
                profile: { lastName: _USER2.lastName }
            };
            chai
                .request(server)
                .put(baseUrl + '/users/' + UNKNOWN_ID)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_PROFILE.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_PROFILE.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.MISSING_PROFILE.CODE_HTTP + '] it should not update the the profile with notice (only role in profile)', done => {
            const user = {
                profile: { role: 'admin' }
            };
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .put(baseUrl + '/users/' + USER_ID)
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_PROFILE.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_PROFILE.MESSAGE[LANG]);
                    expect(res.body, 'Bad notice').to.have.property('notices').to.be.a('array').to.have.length(1);
                    expect(res.body.notices[0], 'Bad notice message').to.include(MESS.USER.UPDATE_PROFILE.NOTICES.INVALID_FIELD.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.MISSING_PROFILE.CODE_HTTP + '] it should not update the the profile with notice (only active in profile)', done => {
            const user = {
                profile: { active: false }
            };
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .put(baseUrl + '/users/' + USER_ID)
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_PROFILE.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_PROFILE.MESSAGE[LANG]);
                    expect(res.body, 'Bad notice').to.have.property('notices').to.be.a('array').to.have.length(1);
                    expect(res.body.notices[0], 'Bad notice message').to.include(MESS.USER.UPDATE_PROFILE.NOTICES.INVALID_FIELD.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.UPDATE_PROFILE.SUCCESS.CODE_HTTP + '] it should update the profile of an user', done => {
            const user = {
                profile: { lastName: _USER2.lastName, firstName: _USER2.firstName }
            };
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .put(baseUrl + '/users/' + USER_ID)
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.UPDATE_PROFILE.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(1);
                    expect(res.body.results[0], 'Bad result email').to.have.property('email').to.be.a('string').to.equal(_USER.email);
                    expect(res.body.results[0], 'Bad result firstName').to.have.property('firstName').to.be.a('string').to.equal(_USER2.firstName);
                    expect(res.body.results[0], 'Bad result lastName').to.have.property('lastName').to.be.a('string').to.equal(_USER2.lastName);
                    expect(res.body.results[0], 'Bad result id').to.have.property('id').to.be.a('string');
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    expect(res.body, 'Bad notice').to.have.property('notices').to.be.null;
                    done();
                });
        });
    });

    describe('/PATCH active/unactive user', () => {
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not unactive an user (auth token missing)', done => {
            chai
                .request(server)
                .patch(baseUrl + '/users/' + USER_ID + '/unactive')
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not unactive an user (bad auth token)', done => {
            const token = 'Bearer: 1' + ADMIN_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/users/' + USER_ID + '/unactive')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.NOT_ADMIN.CODE_HTTP + '] it should not unactive user (user auth token)', done => {
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/users/' + USER_ID + '/unactive')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.NOT_ADMIN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.NOT_ADMIN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.SYSTEM.DB.ERRORS.INVALID_ID.CODE_HTTP + '] it should not unactive an user (invalid format user id)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/users/' + INVALID_ID + '/unactive')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.SYSTEM.DB.ERRORS.INVALID_ID.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.SYSTEM.DB.ERRORS.INVALID_ID.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_ID.CODE_HTTP + '] it should not unactive an user (unknown user id)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/users/' + UNKNOWN_ID + '/unactive')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_ID.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_ID.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.DELETE.SUCCESS.CODE_HTTP + '] it should unactive an user', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/users/' + USER_ID + '/unactive')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.UPDATE_PROFILE.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.USER.DELETE.SUCCESS.CODE_HTTP + '] it should active an user', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/users/' + USER_ID + '/active')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.UPDATE_PROFILE.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
    });

    describe('/PATCH change user password', () => {
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not change an user password (auth token missing)', done => {
            const user = {
                password: _USER2.password
            };
            chai
                .request(server)
                .patch(baseUrl + '/users/' + USER_ID + '/change-password')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not change an user password (bad auth token)', done => {
            const token = 'Bearer: 1' + ADMIN_TOKEN;
            const user = {
                password: _USER2.password
            };
            chai
                .request(server)
                .patch(baseUrl + '/users/' + USER_ID + '/change-password')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.NOT_ADMIN.CODE_HTTP + '] it should not change an user password (user auth token)', done => {
            const token = 'Bearer: ' + USER_TOKEN;
            const user = {
                password: _USER2.password
            };
            chai
                .request(server)
                .patch(baseUrl + '/users/' + USER_ID + '/change-password')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.NOT_ADMIN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.NOT_ADMIN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.SYSTEM.DB.ERRORS.INVALID_ID.CODE_HTTP + '] it should not change an user password (invalid format user id)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            const user = {
                password: _USER2.password
            };
            chai
                .request(server)
                .patch(baseUrl + '/users/' + INVALID_ID + '/change-password')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.SYSTEM.DB.ERRORS.INVALID_ID.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.SYSTEM.DB.ERRORS.INVALID_ID.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_ID.CODE_HTTP + '] it should not change an user password (unknown user id)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            const user = {
                password: _USER2.password
            };
            chai
                .request(server)
                .patch(baseUrl + '/users/' + UNKNOWN_ID + '/change-password')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_ID.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_ID.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.MISSING_PASSWORD.CODE_HTTP + '] it should change an user (password missing)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            const user = {
                password: _USER2.password
            };
            chai
                .request(server)
                .patch(baseUrl + '/users/' + USER_ID + '/change-password')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_PASSWORD.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_PASSWORD.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_PASSWORD.CODE_HTTP + '] it should change an user (password too short)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            const user = {
                password: INVALID_PASSWORD
            };
            chai
                .request(server)
                .patch(baseUrl + '/users/' + USER_ID + '/change-password')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_PASSWORD.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_PASSWORD.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.UPDATE_PROFILE.SUCCESS.CODE_HTTP + '] it should change an user password', done => {
            const user = {
                password: _USER2.password
            };
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/users/' + USER_ID + '/change-password')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.UPDATE_PROFILE.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
    });

    describe('/PATCH add/remove admin role', () => {
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not add admin role to an user (auth token missing)', done => {
            chai
                .request(server)
                .patch(baseUrl + '/users/' + USER_ID + '/add-admin-role')
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not add admin role to an user (bad auth token)', done => {
            const token = 'Bearer: 1' + ADMIN_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/users/' + USER_ID + '/add-admin-role')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.NOT_ADMIN.CODE_HTTP + '] it should not add admin role to user (user auth token)', done => {
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/users/' + USER_ID + '/add-admin-role')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.NOT_ADMIN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.NOT_ADMIN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.SYSTEM.DB.ERRORS.INVALID_ID.CODE_HTTP + '] it should not add admin role to an user (invalid format user id)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/users/' + INVALID_ID + '/add-admin-role')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.SYSTEM.DB.ERRORS.INVALID_ID.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.SYSTEM.DB.ERRORS.INVALID_ID.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_ID.CODE_HTTP + '] it should not add admin role to an user (unknown user id)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/users/' + UNKNOWN_ID + '/add-admin-role')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_ID.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.ADMIN.ERRORS.INVALID_USER.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.DELETE.SUCCESS.CODE_HTTP + '] it should add admin role to an user', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/users/' + USER_ID + '/add-admin-role')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.UPDATE_PROFILE.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.USER.DELETE.SUCCESS.CODE_HTTP + '] it should remove admin role to an user', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/users/' + USER_ID + '/remove-admin-role')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.UPDATE_PROFILE.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
    });

    describe('/DELETE delete user', () => {
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not delete an user (auth token missing)', done => {
            chai
                .request(server)
                .delete(baseUrl + '/users/' + USER_ID)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.DELETE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not delete an user (bad auth token)', done => {
            const token = 'Bearer: 1' + ADMIN_TOKEN;
            chai
                .request(server)
                .delete(baseUrl + '/users/' + USER_ID)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.DELETE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.NOT_ADMIN.CODE_HTTP + '] it should not delete user (user auth token)', done => {
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .delete(baseUrl + '/users/' + USER_ID)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.NOT_ADMIN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.DELETE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.NOT_ADMIN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.SYSTEM.DB.ERRORS.INVALID_ID.CODE_HTTP + '] it should not delete an user (invalid format user id)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .delete(baseUrl + '/users/' + INVALID_ID)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.SYSTEM.DB.ERRORS.INVALID_ID.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.DELETE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.SYSTEM.DB.ERRORS.INVALID_ID.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_ID.CODE_HTTP + '] it should not delete an user (unknown user id)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .delete(baseUrl + '/users/' + UNKNOWN_ID)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_ID.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.DELETE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_ID.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.DELETE.SUCCESS.CODE_HTTP + '] it should delete an user', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .delete(baseUrl + '/users/' + USER_ID)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.DELETE.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.empty;
                    done();
                });
        });
    });

    describe('/GET logs', () => {
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not retrieve all logs (auth token missing)', done => {
            chai
                .request(server)
                .get(baseUrl + '/logs/')
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not retrieve all logs (bad auth token)', done => {
            const token = 'Bearer: 1' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.NOT_ADMIN.CODE_HTTP + '] it should not retrieve all logs (user auth token)', done => {
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.NOT_ADMIN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.NOT_ADMIN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should retrieve all logs', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/?range=0,60')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(206);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array');
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    LOG_ID = res.body.results[0].id;
                    done();
                });
        });
    });

    describe('/GET log', () => {
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not retrieve one log (auth token missing)', done => {
            chai
                .request(server)
                .get(baseUrl + '/logs/' + LOG_ID)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ONE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not retrieve one log (bad auth token)', done => {
            const token = 'Bearer: 1' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/' + LOG_ID)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ONE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.NOT_ADMIN.CODE_HTTP + '] it should not retrieve one log (user auth token)', done => {
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/' + LOG_ID)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.NOT_ADMIN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ONE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.NOT_ADMIN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.SYSTEM.DB.ERRORS.INVALID_ID.CODE_HTTP + '] it should not retrieve one log (invalid format log id)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/' + INVALID_ID)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.SYSTEM.DB.ERRORS.INVALID_ID.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ONE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.SYSTEM.DB.ERRORS.INVALID_ID.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_ID.CODE_HTTP + '] it should not retrieve one log (unknown log id)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/' + UNKNOWN_ID)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.LOG.ERRORS.INVALID_ID.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ONE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.ERRORS.INVALID_ID.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should retrieve one log', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/' + LOG_ID)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ONE.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(1);
                    expect(res.body.results[0], 'Log req type non conforme').to.have.property('req').to.be.a('string');
                    expect(res.body.results[0], 'Log res type non conforme').to.have.property('res').to.be.a('string');
                    expect(res.body.results[0], 'Action non conforme').to.have.property('action').to.be.a('string')
                    expect(res.body.results[0], 'Success non conforme').to.have.property('success').to.be.a('boolean');
                    expect(res.body.results[0], 'reqMethod non conforme').to.have.property('reqMethod').to.be.a('string');
                    expect(res.body.results[0], 'reqUrl non conforme').to.have.property('reqUrl').to.be.a('string');
                    expect(res.body.results[0], 'reqUserAgent non conforme').to.have.property('reqUserAgent').to.be.a('string');
                    expect(res.body.results[0], 'reqIp non conforme').to.have.property('reqIp').to.be.a('string');
                    expect(res.body.results[0], 'resHttpCode non conforme').to.have.property('resHttpCode').to.be.a('number');
                    expect(res.body.results[0], 'resTime non conforme').to.have.property('resTime').to.be.a('number');
                    expect(res.body.results[0], 'error non conforme').to.have.property('error');
                    expect(res.body.results[0], 'date non conforme').to.have.property('date');
                    expect(res.body.results[0], 'user non conforme').to.have.property('user');
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
    });

});
