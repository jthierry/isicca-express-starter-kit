process.env.NODE_ENV = 'testing';
const config = require('config');
const LANG = config.app.LANG.toUpperCase();

const User = require('../models/').User;
const Log = require('../models/').Log;

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const expect = chai.expect;

const MESS = require('../config/messages');

chai.use(chaiHttp);

const baseUrl = '/api/' + config.app.API_VERSION;

const _USER = {
    email: 'user@test.com',
    password: 'P@ssw0rd2',
    firstName: 'prénom2',
    lastName: 'nom2',
    role: 'user'
};
const _USER2 = {
    email: 'user2@test.com',
    password: 'P@ssw0rd2',
    firstName: 'prénom2',
    lastName: 'nom2',
    role: 'user'
};

const UNKNOWN_ID = '11111111-1111-1111-1111-111111111111';
let USER_ID;
let USER_TOKEN;
let USER_LOGID;
let USER2_ID;
let USER2_TOKEN;
let USER2_LOGID;

describe('Logs commands', () => {
    before(async () => {
        await Log.destroy({
            where: {},
        }, err => { });
        await User.destroy({
            where: {},
        }, err => { });
    });

    /*
    * Preparing users
    */
    describe('/POST register & login', () => {
        it('[' + MESS.USER.REGISTER.SUCCESS.CODE_HTTP + '] it should register my first user ', done => {
            const user = { ..._USER };
            chai
                .request(server)
                .post(baseUrl + '/auth/register')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.REGISTER.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad Response').to.be.empty;
                    done();
                });
        });
        it('[' + MESS.USER.LOGIN.SUCCESS.CODE_HTTP + '] it should login my first user', done => {
            const user = { ..._USER };
            chai
                .request(server)
                .post(baseUrl + '/auth/login')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.LOGIN.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(1);
                    expect(res.body.results[0], 'Bad result').to.be.a('object');
                    expect(res.body.results[0], 'Bad result token').to.have.property('token').to.be.a('string');
                    expect(res.body.results[0], 'Bad result expires').to.have.property('expiresIn').to.be.a('number').to.equal(config.user.JWT_EXPIRATIONTIME_HOUR * 60 * 60);
                    expect(res.body.results[0], 'Bad result profile').to.have.property('profile').to.be.a('object');
                    expect(res.body.results[0].profile, 'Bad result profile email').to.have.property('email').to.be.a('string').to.equal(_USER.email);
                    expect(res.body.results[0].profile, 'Bad result profile firstName').to.have.property('firstName').to.be.a('string').to.equal(_USER.firstName);
                    expect(res.body.results[0].profile, 'Bad result profile lastName').to.have.property('lastName').to.be.a('string').to.equal(_USER.lastName);
                    expect(res.body.results[0].profile, 'Bad result profile id').to.have.property('id').to.be.a('string');
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    USER_TOKEN = res.body.results[0].token;
                    USER_ID = res.body.results[0].profile.id;
                    done();
                });
        });
        it('[' + MESS.USER.REGISTER.SUCCESS.CODE_HTTP + '] it should register my second user ', done => {
            const user = { ..._USER2 };
            chai
                .request(server)
                .post(baseUrl + '/auth/register')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.REGISTER.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad Response').to.be.empty;
                    done();
                });
        });
        it('[' + MESS.USER.LOGIN.SUCCESS.CODE_HTTP + '] it should login my second user', done => {
            const user = { ..._USER2 };
            chai
                .request(server)
                .post(baseUrl + '/auth/login')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.LOGIN.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.equal(MESS.USER.LOGIN.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(1);
                    expect(res.body.results[0], 'Bad result').to.be.a('object');
                    expect(res.body.results[0], 'Bad result token').to.have.property('token').to.be.a('string');
                    expect(res.body.results[0], 'Bad result expires').to.have.property('expiresIn').to.be.a('number').to.equal(config.user.JWT_EXPIRATIONTIME_HOUR * 60 * 60);
                    expect(res.body.results[0], 'Bad result profile').to.have.property('profile').to.be.a('object');
                    expect(res.body.results[0].profile, 'Bad result profile email').to.have.property('email').to.be.a('string').to.equal(_USER2.email);
                    expect(res.body.results[0].profile, 'Bad result profile firstName').to.have.property('firstName').to.be.a('string').to.equal(_USER2.firstName);
                    expect(res.body.results[0].profile, 'Bad result profile lastName').to.have.property('lastName').to.be.a('string').to.equal(_USER2.lastName);
                    expect(res.body.results[0].profile, 'Bad result profile id').to.have.property('id').to.be.a('string');
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    USER2_TOKEN = res.body.results[0].token;
                    USER2_ID = res.body.results[0].profile.id;
                    done();
                });
        });
    });

    describe('/GET logs', () => {
        it('[' + MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP + '] it should not fetch all logs (auth token missing)', done => {
            chai
                .request(server)
                .get(baseUrl + '/my/logs')
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not fetch all logs (Bad auth token)', done => {
            const token = 'Bearer: 1' + USER_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/my/logs')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should fetch all my user logs (2 result)', done => {
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/my/logs')
                .set('Authorization', token)
                .end((err, res) => {
                    //console.log(res);
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(2);
                    expect(res.body, 'Bad results message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    USER_LOGID = res.body.results[0].id;
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should fetch all my user logs (3 results)', done => {
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/my/logs')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(2);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should fetch all my user2 logs (2 result)', done => {
            const token = 'Bearer: ' + USER2_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/my/logs')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(2);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    USER2_LOGID = res.body.results[0].id;
                    done();
                });
        });
    });

    describe('/GET log', () => {
        it('[' + MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP + '] it should not fetch one log (auth token missing)', done => {
            chai
                .request(server)
                .get(baseUrl + '/my/logs/' + USER_LOGID)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ONE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not fetch one log (auth token missing)', done => {
            const token = 'Bearer: 1' + USER_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/my/logs/' + USER_LOGID)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ONE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.SYSTEM.DB.ERRORS.INVALID_ID.CODE_HTTP + '] it should not fetch one log (invalid format log id)', done => {
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/my/logs/1')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.SYSTEM.DB.ERRORS.INVALID_ID.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ONE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.SYSTEM.DB.ERRORS.INVALID_ID.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.LOG.ERRORS.INVALID_ID.CODE_HTTP + '] it should not fetch one log (unknown log id)', done => {
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/my/logs/' + UNKNOWN_ID)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.LOG.ERRORS.INVALID_ID.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ONE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.ERRORS.INVALID_ID.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ONE.SUCCESS.CODE_HTTP + '] it should fetch one user log', done => {
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/my/logs/' + USER_LOGID)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.LOG.FETCH_ONE.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ONE.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(1);
                    expect(res.body.results[0], 'Log req type non conforme').to.have.property('req').to.be.a('string');
                    expect(res.body.results[0], 'Log res type non conforme').to.have.property('res').to.be.a('string');
                    expect(res.body.results[0], 'Action non conforme').to.have.property('action').to.be.a('string')
                    expect(res.body.results[0], 'Success non conforme').to.have.property('success').to.be.a('boolean');
                    expect(res.body.results[0], 'reqMethod non conforme').to.have.property('reqMethod').to.be.a('string');
                    expect(res.body.results[0], 'reqUrl non conforme').to.have.property('reqUrl').to.be.a('string');
                    expect(res.body.results[0], 'reqUserAgent non conforme').to.have.property('reqUserAgent').to.be.a('string');
                    expect(res.body.results[0], 'reqIp non conforme').to.have.property('reqIp').to.be.a('string');
                    expect(res.body.results[0], 'resHttpCode non conforme').to.have.property('resHttpCode').to.be.a('number');
                    expect(res.body.results[0], 'resTime non conforme').to.have.property('resTime').to.be.a('number');
                    expect(res.body.results[0], 'error non conforme').to.have.property('error').to.be.null;
                    expect(res.body.results[0], 'date non conforme').to.have.property('date');
                    expect(res.body.results[0], 'user non conforme').to.have.property('user').to.be.a('string');
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ONE.SUCCESS.CODE_HTTP + '] it should fetch one user2 log', done => {
            const token = 'Bearer: ' + USER2_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/my/logs/' + USER2_LOGID)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.LOG.FETCH_ONE.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ONE.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(1);
                    expect(res.body.results[0], 'Log req type non conforme').to.have.property('req').to.be.a('string');
                    expect(res.body.results[0], 'Log res type non conforme').to.have.property('res').to.be.a('string');
                    expect(res.body.results[0], 'Action non conforme').to.have.property('action').to.be.a('string')
                    expect(res.body.results[0], 'Success non conforme').to.have.property('success').to.be.a('boolean');
                    expect(res.body.results[0], 'reqMethod non conforme').to.have.property('reqMethod').to.be.a('string');
                    expect(res.body.results[0], 'reqUrl non conforme').to.have.property('reqUrl').to.be.a('string');
                    expect(res.body.results[0], 'reqUserAgent non conforme').to.have.property('reqUserAgent').to.be.a('string');
                    expect(res.body.results[0], 'reqIp non conforme').to.have.property('reqIp').to.be.a('string');
                    expect(res.body.results[0], 'resHttpCode non conforme').to.have.property('resHttpCode').to.be.a('number');
                    expect(res.body.results[0], 'resTime non conforme').to.have.property('resTime').to.be.a('number');
                    expect(res.body.results[0], 'error non conforme').to.have.property('error').to.be.null;
                    expect(res.body.results[0], 'date non conforme').to.have.property('date');
                    expect(res.body.results[0], 'user non conforme').to.have.property('user').to.be.a('string');
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });

    });
});