process.env.NODE_ENV = 'testing';
const config = require('config');
const LANG = config.app.LANG.toUpperCase();

const User = require('../models/').User;
const Log = require('../models/').Log;

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const expect = chai.expect;

const MESS = require('../config/messages');

chai.use(chaiHttp);

const baseUrl = '/api/' + config.app.API_VERSION;

const _USER = {
    email: 'test@test.com',
    password: 'P@ssw0rd',
    firstName: 'prénom',
    lastName: 'nom',
    role: 'user'
};
const _USER2 = {
    email: 'test2@test.com',
    password: 'P@ssw0rd2',
    firstName: 'prénom2',
    lastName: 'nom2',
    role: 'user'
};
const INVALID_PASSWORD = 'P@ssw0d';
const UNKNOWN_ID = '111111111111111111111111';
let USER_TOKEN;
let USER_ID;

describe('User Authentification', async () => {
    before(async () => {
        await Log.destroy({
            where: {},
        }, err => { });
        await User.destroy({
            where: {},
        }, err => { });
    });

    /*
    * Test the /register route
    */
    describe('/POST register', () => {
        it('[' + MESS.USER.ERRORS.MISSING_EMAIL.CODE_HTTP + '] it should not register an user (missing email)', done => {
            const user = { ..._USER };
            delete user.email;
            chai
                .request(server)
                .post(baseUrl + '/auth/register')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_EMAIL.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.REGISTER.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_EMAIL.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_EMAIL.CODE_HTTP + '] it should not register an user (invalid email)', done => {
            const user = { ..._USER };
            user.email = _USER.email + '1';
            chai
                .request(server)
                .post(baseUrl + '/auth/register')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_EMAIL.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.REGISTER.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_EMAIL.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.MISSING_PASSWORD.CODE_HTTP + '] it should not register an user (missing password)', done => {
            const user = { ..._USER };
            delete user.password;
            chai
                .request(server)
                .post(baseUrl + '/auth/register')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_PASSWORD.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.REGISTER.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_PASSWORD.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_PASSWORD.CODE_HTTP + '] it should not register an user (password too short)', done => {
            const user = { ..._USER };
            user.password = INVALID_PASSWORD;
            chai
                .request(server)
                .post(baseUrl + '/auth/register')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_PASSWORD.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.REGISTER.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_PASSWORD.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_PASSWORD.CODE_HTTP + '] it should not register an user (password without number)', done => {
            const user = { ..._USER };
            user.password = '@Zertyuiopqsdf';
            chai
                .request(server)
                .post(baseUrl + '/auth/register')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_PASSWORD.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.REGISTER.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_PASSWORD.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_PASSWORD.CODE_HTTP + '] it should not register an user (password without special caractere)', done => {
            const user = { ..._USER };
            user.password = '0Zertyuiopqsdf';
            chai
                .request(server)
                .post(baseUrl + '/auth/register')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_PASSWORD.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.REGISTER.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_PASSWORD.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_PASSWORD.CODE_HTTP + '] it should not register an user (password without maj caractere)', done => {
            const user = { ..._USER };
            user.password = '@0zertyuiopqsdf';
            chai
                .request(server)
                .post(baseUrl + '/auth/register')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_PASSWORD.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.REGISTER.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_PASSWORD.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_PASSWORD.CODE_HTTP + '] it should not register an user (password without min caractere)', done => {
            const user = { ..._USER };
            user.password = '@ZERTYUIOP0';
            chai
                .request(server)
                .post(baseUrl + '/auth/register')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_PASSWORD.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.REGISTER.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_PASSWORD.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.REGISTER.SUCCESS.CODE_HTTP + '] it should register an user ', done => {
            const user = { ..._USER };
            chai
                .request(server)
                .post(baseUrl + '/auth/register')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.REGISTER.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad Response').to.be.empty;
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.EXISTING_EMAIL.CODE_HTTP + '] it should not register an user (existing email)', done => {
            const user = { ..._USER };
            chai
                .request(server)
                .post(baseUrl + '/auth/register')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.EXISTING_EMAIL.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.REGISTER.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.EXISTING_EMAIL.MESSAGE[LANG]);
                    done();
                });
        });
    });
    /*
    * Test the /login route
    */
    describe('/POST login', () => {
        it('[' + MESS.USER.ERRORS.MISSING_EMAIL.CODE_HTTP + '] it should not login my user (missing email)', done => {
            const user = {
                password: _USER.password
            };
            chai
                .request(server)
                .post(baseUrl + '/auth/login')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_EMAIL.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.LOGIN.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_EMAIL.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_EMAIL.CODE_HTTP + '] it should not login my user (invalid email)', done => {
            const user = {
                email: _USER.email + '1',
                password: _USER.password
            };
            chai
                .request(server)
                .post(baseUrl + '/auth/login')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_EMAIL.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.LOGIN.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_EMAIL.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.MISSING_PASSWORD.CODE_HTTP + '] it should not login my user (missing password)', done => {
            const user = {
                email: _USER.email
            };
            chai
                .request(server)
                .post(baseUrl + '/auth/login')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_PASSWORD.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.LOGIN.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_PASSWORD.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_PASSWORD.CODE_HTTP + '] it should not login my user (password too short)', done => {
            const user = {
                email: _USER.email,
                password: INVALID_PASSWORD
            };
            chai
                .request(server)
                .post(baseUrl + '/auth/login')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_PASSWORD.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.LOGIN.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_PASSWORD.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_CREDDENTIALS.CODE_HTTP + '] it should not login my user (bad email)', done => {
            const user = {
                email: '1' + _USER.email,
                password: _USER.password
            };
            chai
                .request(server)
                .post(baseUrl + '/auth/login')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_CREDDENTIALS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.LOGIN.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_CREDDENTIALS.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_CREDDENTIALS.CODE_HTTP + '] it should not login my user (bad password)', done => {
            const user = {
                email: _USER.email,
                password: _USER.password + '1'
            };
            chai
                .request(server)
                .post(baseUrl + '/auth/login')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_CREDDENTIALS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.LOGIN.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_CREDDENTIALS.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_CREDDENTIALS.CODE_HTTP + '] it should not login my user (bad email + bad password)', done => {
            const user = {
                email: '1' + _USER.email,
                password: _USER.password + '1'
            };
            chai
                .request(server)
                .post(baseUrl + '/auth/login')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_CREDDENTIALS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.LOGIN.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_CREDDENTIALS.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.LOGIN.SUCCESS.CODE_HTTP + '] it should login my user', done => {
            const user = { ..._USER };
            chai
                .request(server)
                .post(baseUrl + '/auth/login')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.LOGIN.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.LOGIN.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(1);
                    expect(res.body.results[0], 'Bad result').to.be.a('object');
                    expect(res.body.results[0], 'Bad result token').to.have.property('token').to.be.a('string');
                    expect(res.body.results[0], 'Bad result expires').to.have.property('expiresIn').to.be.a('number').to.equal(config.user.JWT_EXPIRATIONTIME_HOUR * 60 * 60);
                    expect(res.body.results[0], 'Bad result profile').to.have.property('profile').to.be.a('object');
                    expect(res.body.results[0].profile, 'Bad result profile email').to.have.property('email').to.be.a('string').to.equal(_USER.email);
                    expect(res.body.results[0].profile, 'Bad result profile firstName').to.have.property('firstName').to.be.a('string').to.equal(_USER.firstName);
                    expect(res.body.results[0].profile, 'Bad result profile lastName').to.have.property('lastName').to.be.a('string').to.equal(_USER.lastName);
                    expect(res.body.results[0].profile, 'Bad result profile id').to.have.property('id');
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    USER_TOKEN = res.body.results[0].token;
                    USER_ID = res.body.results[0].profile.id;
                    done();
                });
        });
    });
});