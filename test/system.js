process.env.NODE_ENV = 'testing';
const config = require('config');
const LANG = config.app.LANG.toUpperCase();

const User = require('../models/').User;
const Log = require('../models/').Log;

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const expect = chai.expect;

const MESS = require('../config/messages');

chai.use(chaiHttp);

const baseUrl = '/api/' + config.app.API_VERSION;

const _ADMIN = {
    email: 'admin@test.com',
    password: 'P@ssw0rd',
    firstName: 'prénom',
    lastName: 'nom',
    role: 'admin'
};
const _USER = {
    email: 'user@test.com',
    password: 'P@ssw0rd',
    firstName: 'prénom',
    lastName: 'nom',
    role: 'user'
};
const _USER2 = {
    email: 'user2@test.com',
    password: 'P@ssw0rd2',
    firstName: 'prénom2',
    lastName: 'nom2',
    role: 'user'
};

const INVALID_PASSWORD = 'P@ssw0d';
const INVALID_ID = '111111'
const UNKNOWN_ID = '11111111-1111-1111-1111-111111111111';
let LOG_ID;
let USER_ID;
let USER_TOKEN;
let USER_LOGID;
let USER2_ID;
let USER2_TOKEN;
let USER2_LOGID;
let ADMIN_ID;
let ADMIN_TOKEN;
let ADMIN_LOGID;

describe('Apps commands', () => {
    before(async () => {
        await Log.destroy({
            where: {},
        }, err => { });
        await User.destroy({
            where: {},
        }, err => { });
    });

    /*
    * Preparing admin & users
    */
    describe('/POST register & login', () => {
        it('[' + MESS.USER.REGISTER.SUCCESS.CODE_HTTP + '] it should register my first user ', done => {
            const user = { ..._USER };
            chai
                .request(server)
                .post(baseUrl + '/auth/register')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.REGISTER.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad Response').to.be.empty;
                    done();
                });
        });
        it('[' + MESS.USER.LOGIN.SUCCESS.CODE_HTTP + '] it should login my first user (metadata check)', done => {
            const user = { ..._USER };
            chai
                .request(server)
                .post(baseUrl + '/auth/login')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.LOGIN.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad metadata').to.have.property('metadata').to.be.a('object');
                    expect(res.body.metadata, 'Bad metadata url').to.have.property('url').to.equal(baseUrl + '/auth/login');
                    expect(res.body.metadata, 'Bad metadata query').to.have.property('query').to.be.a('object');
                    expect(res.body.metadata.query, 'Bad metadata query filter').to.not.have.property('filter');
                    expect(res.body.metadata.query, 'Bad metadata query select').to.not.have.property('select');
                    expect(res.body.metadata.query, 'Bad metadata query sort').to.not.have.property('sort');
                    expect(res.body.metadata.query, 'Bad metadata query limit').to.not.have.property('limit');
                    expect(res.body.metadata.query, 'Bad metadata query range').to.not.have.property('range');
                    expect(res.body.metadata, 'Bad metadata links').to.have.property('links').to.be.a('array').to.have.length(1);
                    expect(res.body.metadata.links[0], 'Bad metadata links profile rel').to.have.property('rel').to.equal('profile');
                    expect(res.body.metadata.links[0], 'Bad metadata links profile href').to.have.property('href').to.equal(baseUrl + '/auth/' + res.body.results[0].profile.id);
                    expect(res.body.metadata.links[0], 'Bad metadata links profile method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata, 'Bad metadata error').to.have.property('error').to.equal(0);
                    expect(res.body.metadata, 'Bad metadata notices').to.have.property('notices').to.equal(0);
                    expect(res.body.metadata, 'Bad metadata results').to.have.property('results').to.equal(1);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(1);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    USER_TOKEN = res.body.results[0].token;
                    USER_ID = res.body.results[0].profile.id;
                    done();
                });
        });
        it('[' + MESS.USER.REGISTER.SUCCESS.CODE_HTTP + '] it should register my admin', done => {
            const user = { ..._ADMIN };
            chai
                .request(server)
                .post(baseUrl + '/auth/register')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.REGISTER.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad Response').to.be.empty;
                    done();
                });
        });
        it('[' + MESS.USER.LOGIN.SUCCESS.CODE_HTTP + '] it should login my admin', done => {
            const user = { ..._ADMIN };
            chai
                .request(server)
                .post(baseUrl + '/auth/login')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.LOGIN.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(1);
                    expect(res.body.results[0], 'Bad result').to.be.a('object');
                    expect(res.body.results[0], 'Bad result token').to.have.property('token').to.be.a('string');
                    expect(res.body.results[0], 'Bad result expires').to.have.property('expiresIn').to.be.a('number').to.equal(config.user.JWT_EXPIRATIONTIME_HOUR * 60 * 60);
                    expect(res.body.results[0], 'Bad result profile').to.have.property('profile').to.be.a('object');
                    expect(res.body.results[0].profile, 'Bad result profile email').to.have.property('email').to.be.a('string').to.equal(_ADMIN.email);
                    expect(res.body.results[0].profile, 'Bad result profile firstName').to.have.property('firstName').to.be.a('string').to.equal(_ADMIN.firstName);
                    expect(res.body.results[0].profile, 'Bad result profile lastName').to.have.property('lastName').to.be.a('string').to.equal(_ADMIN.lastName);
                    expect(res.body.results[0].profile, 'Bad result profile id').to.have.property('id').to.be.a('string');
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    ADMIN_TOKEN = res.body.results[0].token;
                    ADMIN_ID = res.body.results[0].profile.id;
                    done();
                });
        });
    });

    /*
    * Test query strings
    */
    describe('/GET logs with query strings', () => {
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should retrieve all logs', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(200);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(4);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    LOG_ID = res.body.results[0].id;
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should retrieve all logs with query sort & select include', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/?sort=resHttpCode(desc)&select=include(resHttpCode)')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body.metadata, 'Bad metadata query').to.have.property('query').to.be.a('object');
                    expect(res.body.metadata.query, 'Bad metadata query filter').to.not.have.property('filter');
                    expect(res.body.metadata.query, 'Bad metadata query select').to.have.property('select').to.be.a('array').to.have.length(2);
                    expect(res.body.metadata.query.select[0], 'Bad metadata query select 0').to.equal('id');
                    expect(res.body.metadata.query.select[1], 'Bad metadata query select 1').to.equal('resHttpCode');
                    expect(res.body.metadata.query, 'Bad metadata query sort').to.have.property('sort').to.be.a('array').to.have.length(1);
                    expect(res.body.metadata.query.sort[0][0], 'Bad metadata query sort 0 key').to.be.equal('resHttpCode');
                    expect(res.body.metadata.query.sort[0][1], 'Bad metadata query sort 0 value').to.be.equal('DESC');
                    expect(res.body.metadata.query, 'Bad metadata query limit').to.not.have.property('limit');
                    expect(res.body.metadata.query, 'Bad metadata query range').to.have.property('range').to.be.a('object');
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array');
                    expect(res.body.results[0].resHttpCode, 'Bad results sorting 0').to.be.equal(201);
                    expect(res.body.results[1].resHttpCode, 'Bad results sorting 1').to.be.equal(201);
                    expect(res.body.results[2].resHttpCode, 'Bad results sorting 2').to.be.equal(200);
                    expect(res.body.results[3].resHttpCode, 'Bad results sorting 3').to.be.equal(200);
                    expect(res.body.results[4].resHttpCode, 'Bad results sorting 4').to.be.equal(200);
                    expect(Object.keys(res.body.results[0]).length, 'Bad results fields').equal(2);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should retrieve all logs with query range & select exclude', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/?select=exclude(id,date,req,res,action,success,reqMethod,reqUrl,reqUserAgent,reqIp,resTime,error,notices)&range=0,3')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res.headers, 'Bad header content-range').to.have.property('content-range').to.equal('0-3/6');
                    expect(res, 'Bad http code').to.have.status(206);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body.metadata, 'Bad metadata query').to.have.property('query').to.be.a('object');
                    expect(res.body.metadata.query, 'Bad metadata query filter').to.not.have.property('filter');
                    expect(res.body.metadata.query, 'Bad metadata query select').to.have.property('select').to.be.a('object').to.have.property('exclude');
                    expect(res.body.metadata.query, 'Bad metadata query sort').to.have.property('sort').to.be.a('array').to.have.length(1);
                    expect(res.body.metadata.query.sort[0][0], 'Bad metadata query sort 0 key').to.be.equal(config.query.SORT_DEFAULT.split('(')[0]);
                    expect(res.body.metadata.query.sort[0][1], 'Bad metadata query sort 0 value').to.be.equal(config.query.SORT_DEFAULT.split('(')[1].slice(0, -1).toUpperCase());
                    expect(res.body.metadata.query, 'Bad metadata query limit').to.not.have.property('limit');
                    expect(res.body.metadata.query, 'Bad metadata query range').to.have.property('range').to.be.a('object');
                    expect(res.body.metadata.query.range, 'Bad metadata query range start').to.have.property('start').to.equal(0);
                    expect(res.body.metadata.query.range, 'Bad metadata query range end').to.have.property('end').to.equal(3);
                    expect(res.body.metadata.query.range, 'Bad metadata query range count').to.have.property('count').to.equal(4);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(4);
                    expect(Object.keys(res.body.results[0]).length, 'Bad results fields').equal(5);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should retrieve all logs with query filter equal', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/?select=include(resHttpCode)&filter=resHttpCode(201|206)')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(200);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(3);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should retrieve all logs with query filter compare', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/?select=include(resHttpCode)&filter=resHttpCode(>200|<205)')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(200);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(2);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should retrieve all logs with query filter user', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/?select=include(resHttpCode)&filter=user(' + USER_ID + ')')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(200);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(2);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should retrieve all logs with query limit', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/?select=include(resHttpCode)&limit=5')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(200);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(5);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
    });

    /*
    * Test query range
    */
   describe('/GET logs with query range', () => {
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should retrieve all logs with query range (11 results)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/?select=include(resHttpCode)&range=0,1')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res.headers, 'Bad header content-range').to.have.property('content-range').to.equal('0-1/11');
                    expect(res, 'Bad http code').to.have.status(206);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body.metadata, 'Bad metadata query').to.have.property('query').to.be.a('object');
                    expect(res.body.metadata.query, 'Bad metadata query filter').to.not.have.property('filter');
                    expect(res.body.metadata.query, 'Bad metadata query select').to.have.property('select').to.be.a('array').to.have.length(2);
                    expect(res.body.metadata.query, 'Bad metadata query sort').to.have.property('sort').to.be.a('array').to.have.length(1);
                    expect(res.body.metadata.query, 'Bad metadata query limit').to.not.have.property('limit');
                    expect(res.body.metadata.query, 'Bad metadata query range').to.have.property('range').to.be.a('object');
                    expect(res.body.metadata.query.range, 'Bad metadata query range start').to.have.property('start').to.equal(0);
                    expect(res.body.metadata.query.range, 'Bad metadata query range end').to.have.property('end').to.equal(1);
                    expect(res.body.metadata.query.range, 'Bad metadata query range count').to.have.property('count').to.equal(2);
                    expect(res.body.metadata, 'Bad metadata links').to.have.property('links').to.be.a('array').to.have.length(3);
                    expect(res.body.metadata.links[0], 'Bad metadata links first rel').to.have.property('rel').to.equal('first');
                    expect(res.body.metadata.links[0], 'Bad metadata links first href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=0,1');
                    expect(res.body.metadata.links[0], 'Bad metadata links first method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[1], 'Bad metadata links next rel').to.have.property('rel').to.equal('next');
                    expect(res.body.metadata.links[1], 'Bad metadata links next href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=2,3');
                    expect(res.body.metadata.links[1], 'Bad metadata links next method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[2], 'Bad metadata links last rel').to.have.property('rel').to.equal('last');
                    expect(res.body.metadata.links[2], 'Bad metadata links last href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=9,10');
                    expect(res.body.metadata.links[2], 'Bad metadata links last method').to.have.property('method').to.equal('GET');
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(2);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should retrieve all logs with query range (12 results)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/?select=include(resHttpCode)&range=0,2')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res.headers, 'Bad header content-range').to.have.property('content-range').to.equal('0-2/12');
                    expect(res, 'Bad http code').to.have.status(206);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body.metadata, 'Bad metadata query').to.have.property('query').to.be.a('object');
                    expect(res.body.metadata.query, 'Bad metadata query filter').to.not.have.property('filter');
                    expect(res.body.metadata.query, 'Bad metadata query select').to.have.property('select').to.be.a('array').to.have.length(2);
                    expect(res.body.metadata.query, 'Bad metadata query sort').to.have.property('sort').to.be.a('array').to.have.length(1);
                    expect(res.body.metadata.query, 'Bad metadata query limit').to.not.have.property('limit');
                    expect(res.body.metadata.query, 'Bad metadata query range').to.have.property('range').to.be.a('object');
                    expect(res.body.metadata.query.range, 'Bad metadata query range start').to.have.property('start').to.equal(0);
                    expect(res.body.metadata.query.range, 'Bad metadata query range end').to.have.property('end').to.equal(2);
                    expect(res.body.metadata.query.range, 'Bad metadata query range count').to.have.property('count').to.equal(3);
                    expect(res.body.metadata, 'Bad metadata links').to.have.property('links').to.be.a('array').to.have.length(3);
                    expect(res.body.metadata.links[0], 'Bad metadata links first rel').to.have.property('rel').to.equal('first');
                    expect(res.body.metadata.links[0], 'Bad metadata links first href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=0,2');
                    expect(res.body.metadata.links[0], 'Bad metadata links first method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[1], 'Bad metadata links next rel').to.have.property('rel').to.equal('next');
                    expect(res.body.metadata.links[1], 'Bad metadata links next href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=3,5');
                    expect(res.body.metadata.links[1], 'Bad metadata links next method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[2], 'Bad metadata links last rel').to.have.property('rel').to.equal('last');
                    expect(res.body.metadata.links[2], 'Bad metadata links last href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=9,11');
                    expect(res.body.metadata.links[2], 'Bad metadata links last method').to.have.property('method').to.equal('GET');
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(3);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should retrieve all logs with query range (13 results)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/?select=include(resHttpCode)&range=0,5')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res.headers, 'Bad header content-range').to.have.property('content-range').to.equal('0-5/13');
                    expect(res, 'Bad http code').to.have.status(206);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body.metadata, 'Bad metadata query').to.have.property('query').to.be.a('object');
                    expect(res.body.metadata.query, 'Bad metadata query filter').to.not.have.property('filter');
                    expect(res.body.metadata.query, 'Bad metadata query select').to.have.property('select').to.be.a('array').to.have.length(2);
                    expect(res.body.metadata.query, 'Bad metadata query sort').to.have.property('sort').to.be.a('array').to.have.length(1);
                    expect(res.body.metadata.query, 'Bad metadata query limit').to.not.have.property('limit');
                    expect(res.body.metadata.query, 'Bad metadata query range').to.have.property('range').to.be.a('object');
                    expect(res.body.metadata.query.range, 'Bad metadata query range start').to.have.property('start').to.equal(0);
                    expect(res.body.metadata.query.range, 'Bad metadata query range end').to.have.property('end').to.equal(5);
                    expect(res.body.metadata.query.range, 'Bad metadata query range count').to.have.property('count').to.equal(6);
                    expect(res.body.metadata, 'Bad metadata links').to.have.property('links').to.be.a('array').to.have.length(3);
                    expect(res.body.metadata.links[0], 'Bad metadata links first rel').to.have.property('rel').to.equal('first');
                    expect(res.body.metadata.links[0], 'Bad metadata links first href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=0,5');
                    expect(res.body.metadata.links[0], 'Bad metadata links first method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[1], 'Bad metadata links next rel').to.have.property('rel').to.equal('next');
                    expect(res.body.metadata.links[1], 'Bad metadata links next href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=6,11');
                    expect(res.body.metadata.links[1], 'Bad metadata links next method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[2], 'Bad metadata links last rel').to.have.property('rel').to.equal('last');
                    expect(res.body.metadata.links[2], 'Bad metadata links last href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=7,12');
                    expect(res.body.metadata.links[2], 'Bad metadata links last method').to.have.property('method').to.equal('GET');
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(6);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should retrieve all logs with query range (14 results)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/?select=include(resHttpCode)&range=1,2')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res.headers, 'Bad header content-range').to.have.property('content-range').to.equal('1-2/14');
                    expect(res, 'Bad http code').to.have.status(206);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body.metadata, 'Bad metadata query').to.have.property('query').to.be.a('object');
                    expect(res.body.metadata.query, 'Bad metadata query filter').to.not.have.property('filter');
                    expect(res.body.metadata.query, 'Bad metadata query select').to.have.property('select').to.be.a('array').to.have.length(2);
                    expect(res.body.metadata.query, 'Bad metadata query sort').to.have.property('sort').to.be.a('array').to.have.length(1);
                    expect(res.body.metadata.query, 'Bad metadata query limit').to.not.have.property('limit');
                    expect(res.body.metadata.query, 'Bad metadata query range').to.have.property('range').to.be.a('object');
                    expect(res.body.metadata.query.range, 'Bad metadata query range start').to.have.property('start').to.equal(1);
                    expect(res.body.metadata.query.range, 'Bad metadata query range end').to.have.property('end').to.equal(2);
                    expect(res.body.metadata.query.range, 'Bad metadata query range count').to.have.property('count').to.equal(2);
                    expect(res.body.metadata, 'Bad metadata links').to.have.property('links').to.be.a('array').to.have.length(4);
                    expect(res.body.metadata.links[0], 'Bad metadata links first rel').to.have.property('rel').to.equal('first');
                    expect(res.body.metadata.links[0], 'Bad metadata links first href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=0,1');
                    expect(res.body.metadata.links[0], 'Bad metadata links first method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[1], 'Bad metadata links previous rel').to.have.property('rel').to.equal('previous');
                    expect(res.body.metadata.links[1], 'Bad metadata links previous href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=0,1');
                    expect(res.body.metadata.links[1], 'Bad metadata links previous method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[2], 'Bad metadata links next rel').to.have.property('rel').to.equal('next');
                    expect(res.body.metadata.links[2], 'Bad metadata links next href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=3,4');
                    expect(res.body.metadata.links[2], 'Bad metadata links next method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[3], 'Bad metadata links last rel').to.have.property('rel').to.equal('last');
                    expect(res.body.metadata.links[3], 'Bad metadata links last href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=12,13');
                    expect(res.body.metadata.links[3], 'Bad metadata links last method').to.have.property('method').to.equal('GET');
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(2);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should retrieve all logs with query range (15 results)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/?select=include(resHttpCode)&range=5,10')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res.headers, 'Bad header content-range').to.have.property('content-range').to.equal('5-10/15');
                    expect(res, 'Bad http code').to.have.status(206);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body.metadata, 'Bad metadata query').to.have.property('query').to.be.a('object');
                    expect(res.body.metadata.query, 'Bad metadata query filter').to.not.have.property('filter');
                    expect(res.body.metadata.query, 'Bad metadata query select').to.have.property('select').to.be.a('array').to.have.length(2);
                    expect(res.body.metadata.query, 'Bad metadata query sort').to.have.property('sort').to.be.a('array').to.have.length(1);
                    expect(res.body.metadata.query, 'Bad metadata query limit').to.not.have.property('limit');
                    expect(res.body.metadata.query, 'Bad metadata query range').to.have.property('range').to.be.a('object');
                    expect(res.body.metadata.query.range, 'Bad metadata query range start').to.have.property('start').to.equal(5);
                    expect(res.body.metadata.query.range, 'Bad metadata query range end').to.have.property('end').to.equal(10);
                    expect(res.body.metadata.query.range, 'Bad metadata query range count').to.have.property('count').to.equal(6);
                    expect(res.body.metadata, 'Bad metadata links').to.have.property('links').to.be.a('array').to.have.length(4);
                    expect(res.body.metadata.links[0], 'Bad metadata links first rel').to.have.property('rel').to.equal('first');
                    expect(res.body.metadata.links[0], 'Bad metadata links first href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=0,5');
                    expect(res.body.metadata.links[0], 'Bad metadata links first method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[1], 'Bad metadata links previous rel').to.have.property('rel').to.equal('previous');
                    expect(res.body.metadata.links[1], 'Bad metadata links previous href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=0,5');
                    expect(res.body.metadata.links[1], 'Bad metadata links previous method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[2], 'Bad metadata links next rel').to.have.property('rel').to.equal('next');
                    expect(res.body.metadata.links[2], 'Bad metadata links next href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=11,16');
                    expect(res.body.metadata.links[2], 'Bad metadata links next method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[3], 'Bad metadata links last rel').to.have.property('rel').to.equal('last');
                    expect(res.body.metadata.links[3], 'Bad metadata links last href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=9,14');
                    expect(res.body.metadata.links[3], 'Bad metadata links last method').to.have.property('method').to.equal('GET');
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(6);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should retrieve all logs with query range (16 results)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/?select=include(resHttpCode)&range=5,10')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res.headers, 'Bad header content-range').to.have.property('content-range').to.equal('5-10/16');
                    expect(res, 'Bad http code').to.have.status(206);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body.metadata, 'Bad metadata query').to.have.property('query').to.be.a('object');
                    expect(res.body.metadata.query, 'Bad metadata query filter').to.not.have.property('filter');
                    expect(res.body.metadata.query, 'Bad metadata query select').to.have.property('select').to.be.a('array').to.have.length(2);
                    expect(res.body.metadata.query, 'Bad metadata query sort').to.have.property('sort').to.be.a('array').to.have.length(1);
                    expect(res.body.metadata.query, 'Bad metadata query limit').to.not.have.property('limit');
                    expect(res.body.metadata.query, 'Bad metadata query range').to.have.property('range').to.be.a('object');
                    expect(res.body.metadata.query.range, 'Bad metadata query range start').to.have.property('start').to.equal(5);
                    expect(res.body.metadata.query.range, 'Bad metadata query range end').to.have.property('end').to.equal(10);
                    expect(res.body.metadata.query.range, 'Bad metadata query range count').to.have.property('count').to.equal(6);
                    expect(res.body.metadata, 'Bad metadata links').to.have.property('links').to.be.a('array').to.have.length(4);
                    expect(res.body.metadata.links[0], 'Bad metadata links first rel').to.have.property('rel').to.equal('first');
                    expect(res.body.metadata.links[0], 'Bad metadata links first href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=0,5');
                    expect(res.body.metadata.links[0], 'Bad metadata links first method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[1], 'Bad metadata links previous rel').to.have.property('rel').to.equal('previous');
                    expect(res.body.metadata.links[1], 'Bad metadata links previous href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=0,5');
                    expect(res.body.metadata.links[1], 'Bad metadata links previous method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[2], 'Bad metadata links next rel').to.have.property('rel').to.equal('next');
                    expect(res.body.metadata.links[2], 'Bad metadata links next href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=11,16');
                    expect(res.body.metadata.links[2], 'Bad metadata links next method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[3], 'Bad metadata links last rel').to.have.property('rel').to.equal('last');
                    expect(res.body.metadata.links[3], 'Bad metadata links last href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=10,15');
                    expect(res.body.metadata.links[3], 'Bad metadata links last method').to.have.property('method').to.equal('GET');
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(6);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should retrieve all logs with query range (17 results)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/?select=include(resHttpCode)&range=10,15')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res.headers, 'Bad header content-range').to.have.property('content-range').to.equal('10-15/17');
                    expect(res, 'Bad http code').to.have.status(206);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body.metadata, 'Bad metadata query').to.have.property('query').to.be.a('object');
                    expect(res.body.metadata.query, 'Bad metadata query filter').to.not.have.property('filter');
                    expect(res.body.metadata.query, 'Bad metadata query select').to.have.property('select').to.be.a('array').to.have.length(2);
                    expect(res.body.metadata.query, 'Bad metadata query sort').to.have.property('sort').to.be.a('array').to.have.length(1);
                    expect(res.body.metadata.query, 'Bad metadata query limit').to.not.have.property('limit');
                    expect(res.body.metadata.query, 'Bad metadata query range').to.have.property('range').to.be.a('object');
                    expect(res.body.metadata.query.range, 'Bad metadata query range start').to.have.property('start').to.equal(10);
                    expect(res.body.metadata.query.range, 'Bad metadata query range end').to.have.property('end').to.equal(15);
                    expect(res.body.metadata.query.range, 'Bad metadata query range count').to.have.property('count').to.equal(6);
                    expect(res.body.metadata, 'Bad metadata links').to.have.property('links').to.be.a('array').to.have.length(3);
                    expect(res.body.metadata.links[0], 'Bad metadata links first rel').to.have.property('rel').to.equal('first');
                    expect(res.body.metadata.links[0], 'Bad metadata links first href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=0,5');
                    expect(res.body.metadata.links[0], 'Bad metadata links first method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[1], 'Bad metadata links previous rel').to.have.property('rel').to.equal('previous');
                    expect(res.body.metadata.links[1], 'Bad metadata links previous href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=4,9');
                    expect(res.body.metadata.links[1], 'Bad metadata links previous method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[2], 'Bad metadata links last rel').to.have.property('rel').to.equal('last');
                    expect(res.body.metadata.links[2], 'Bad metadata links last href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=11,16');
                    expect(res.body.metadata.links[2], 'Bad metadata links last method').to.have.property('method').to.equal('GET');
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(6);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should retrieve all logs with query range (18 results)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/?select=include(resHttpCode)&range=15,20')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res.headers, 'Bad header content-range').to.have.property('content-range').to.equal('15-17/18');
                    expect(res, 'Bad http code').to.have.status(206);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body.metadata, 'Bad metadata query').to.have.property('query').to.be.a('object');
                    expect(res.body.metadata.query, 'Bad metadata query filter').to.not.have.property('filter');
                    expect(res.body.metadata.query, 'Bad metadata query select').to.have.property('select').to.be.a('array').to.have.length(2);
                    expect(res.body.metadata.query, 'Bad metadata query sort').to.have.property('sort').to.be.a('array').to.have.length(1);
                    expect(res.body.metadata.query, 'Bad metadata query limit').to.not.have.property('limit');
                    expect(res.body.metadata.query, 'Bad metadata query range').to.have.property('range').to.be.a('object');
                    expect(res.body.metadata.query.range, 'Bad metadata query range start').to.have.property('start').to.equal(15);
                    expect(res.body.metadata.query.range, 'Bad metadata query range end').to.have.property('end').to.equal(20);
                    expect(res.body.metadata.query.range, 'Bad metadata query range count').to.have.property('count').to.equal(6);
                    expect(res.body.metadata, 'Bad metadata links').to.have.property('links').to.be.a('array').to.have.length(3);
                    expect(res.body.metadata.links[0], 'Bad metadata links first rel').to.have.property('rel').to.equal('first');
                    expect(res.body.metadata.links[0], 'Bad metadata links first href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=0,5');
                    expect(res.body.metadata.links[0], 'Bad metadata links first method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[1], 'Bad metadata links previous rel').to.have.property('rel').to.equal('previous');
                    expect(res.body.metadata.links[1], 'Bad metadata links previous href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=9,14');
                    expect(res.body.metadata.links[1], 'Bad metadata links previous method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[2], 'Bad metadata links last rel').to.have.property('rel').to.equal('last');
                    expect(res.body.metadata.links[2], 'Bad metadata links last href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=12,17');
                    expect(res.body.metadata.links[2], 'Bad metadata links last method').to.have.property('method').to.equal('GET');
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(3);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should retrieve all logs with query range (19 results)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/?select=include(resHttpCode)&range=0,0')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res.headers, 'Bad header content-range').to.have.property('content-range').to.equal('0-0/19');
                    expect(res, 'Bad http code').to.have.status(206);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body.metadata, 'Bad metadata query').to.have.property('query').to.be.a('object');
                    expect(res.body.metadata.query, 'Bad metadata query filter').to.not.have.property('filter');
                    expect(res.body.metadata.query, 'Bad metadata query select').to.have.property('select').to.be.a('array').to.have.length(2);
                    expect(res.body.metadata.query, 'Bad metadata query sort').to.have.property('sort').to.be.a('array').to.have.length(1);
                    expect(res.body.metadata.query, 'Bad metadata query limit').to.not.have.property('limit');
                    expect(res.body.metadata.query, 'Bad metadata query range').to.have.property('range').to.be.a('object');
                    expect(res.body.metadata.query.range, 'Bad metadata query range start').to.have.property('start').to.equal(0);
                    expect(res.body.metadata.query.range, 'Bad metadata query range end').to.have.property('end').to.equal(0);
                    expect(res.body.metadata.query.range, 'Bad metadata query range count').to.have.property('count').to.equal(1);
                    expect(res.body.metadata, 'Bad metadata links').to.have.property('links').to.be.a('array').to.have.length(3);
                    expect(res.body.metadata.links[0], 'Bad metadata links first rel').to.have.property('rel').to.equal('first');
                    expect(res.body.metadata.links[0], 'Bad metadata links first href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=0,0');
                    expect(res.body.metadata.links[0], 'Bad metadata links first method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[1], 'Bad metadata links next rel').to.have.property('rel').to.equal('next');
                    expect(res.body.metadata.links[1], 'Bad metadata links next href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=1,1');
                    expect(res.body.metadata.links[1], 'Bad metadata links next method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[2], 'Bad metadata links last rel').to.have.property('rel').to.equal('last');
                    expect(res.body.metadata.links[2], 'Bad metadata links last href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=18,18');
                    expect(res.body.metadata.links[2], 'Bad metadata links last method').to.have.property('method').to.equal('GET');
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(1);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.LOG.FETCH_ALL.SUCCESS.CODE_HTTP + '] it should retrieve all logs with query range with max range notice (20 results)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/?select=include(resHttpCode)&range=0,70')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res.headers, 'Bad header content-range').to.have.property('content-range').to.equal('0-19/20');
                    expect(res, 'Bad http code').to.have.status(200);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.SUCCESS.MESSAGE[LANG]);
                    expect(res.body.metadata, 'Bad metadata query').to.have.property('query').to.be.a('object');
                    expect(res.body.metadata.query, 'Bad metadata query filter').to.not.have.property('filter');
                    expect(res.body.metadata.query, 'Bad metadata query select').to.have.property('select').to.be.a('array').to.have.length(2);
                    expect(res.body.metadata.query, 'Bad metadata query sort').to.have.property('sort').to.be.a('array').to.have.length(1);
                    expect(res.body.metadata.query, 'Bad metadata query limit').to.not.have.property('limit');
                    expect(res.body.metadata.query, 'Bad metadata query range').to.have.property('range').to.be.a('object');
                    expect(res.body.metadata.query.range, 'Bad metadata query range start').to.have.property('start').to.equal(0);
                    expect(res.body.metadata.query.range, 'Bad metadata query range end').to.have.property('end').to.equal(19);
                    expect(res.body.metadata.query.range, 'Bad metadata query range count').to.have.property('count').to.equal(20);
                    expect(res.body.metadata, 'Bad metadata links').to.have.property('links').to.be.a('array').to.have.length(2);
                    expect(res.body.metadata.links[0], 'Bad metadata links first rel').to.have.property('rel').to.equal('first');
                    expect(res.body.metadata.links[0], 'Bad metadata links first href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=0,19');
                    expect(res.body.metadata.links[0], 'Bad metadata links first method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[1], 'Bad metadata links last rel').to.have.property('rel').to.equal('last');
                    expect(res.body.metadata.links[1], 'Bad metadata links last href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=0,19');
                    expect(res.body.metadata.links[1], 'Bad metadata links last method').to.have.property('method').to.equal('GET');
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(20);
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    expect(res.body, 'Bad notices').to.have.property('notices').to.be.a('array').to.have.length(1);
                    expect(res.body.notices[0], 'Bad notice message').to.includes(MESS.SYSTEM.QUERY.RANGE_TOO_BIG.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.SYSTEM.QUERY.RANGE_INVALID_VALUE.CODE_HTTP + '] it should not retrieve all logs with query range (range start bigger than results) (21 results)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/?select=include(resHttpCode)&range=30,40')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res.headers, 'Bad header content-range').to.have.property('content-range').to.equal('*/21');
                    expect(res, 'Bad http code').to.have.status(MESS.SYSTEM.QUERY.RANGE_INVALID_VALUE.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.ERROR.MESSAGE[LANG]);
                    expect(res.body.metadata, 'Bad metadata query').to.have.property('query').to.be.a('object');
                    expect(res.body.metadata.query, 'Bad metadata query filter').to.not.have.property('filter');
                    expect(res.body.metadata.query, 'Bad metadata query select').to.have.property('select').to.be.a('array').to.have.length(2);
                    expect(res.body.metadata.query, 'Bad metadata query sort').to.have.property('sort').to.be.a('array').to.have.length(1);
                    expect(res.body.metadata.query, 'Bad metadata query limit').to.not.have.property('limit');
                    expect(res.body.metadata.query, 'Bad metadata query range').to.have.property('range').to.be.a('object');
                    expect(res.body.metadata.query.range, 'Bad metadata query range start').to.have.property('start').to.equal(30);
                    expect(res.body.metadata.query.range, 'Bad metadata query range end').to.have.property('end').to.equal(40);
                    expect(res.body.metadata.query.range, 'Bad metadata query range count').to.have.property('count').to.equal(11);
                    expect(res.body.metadata, 'Bad metadata links').to.have.property('links').to.be.a('array').to.have.length(2);
                    expect(res.body.metadata.links[0], 'Bad metadata links first rel').to.have.property('rel').to.equal('first');
                    expect(res.body.metadata.links[0], 'Bad metadata links first href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=0,10');
                    expect(res.body.metadata.links[0], 'Bad metadata links first method').to.have.property('method').to.equal('GET');
                    expect(res.body.metadata.links[1], 'Bad metadata links last rel').to.have.property('rel').to.equal('last');
                    expect(res.body.metadata.links[1], 'Bad metadata links last href').to.have.property('href').to.equal(baseUrl + '/logs/?select=include(resHttpCode)&range=10,20');
                    expect(res.body.metadata.links[1], 'Bad metadata links last method').to.have.property('method').to.equal('GET');
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.equal(MESS.SYSTEM.QUERY.RANGE_INVALID_VALUE.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.SYSTEM.QUERY.RANGE_INVALID_START.CODE_HTTP + '] it should not retrieve all logs with query range (range start bigger than range end) (22 results)', done => {
            const token = 'Bearer: ' + ADMIN_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/logs/?select=include(resHttpCode)&range=20,10')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res.headers, 'Bad header content-range').to.not.have.property('content-range');
                    expect(res, 'Bad http code').to.have.status(MESS.SYSTEM.QUERY.RANGE_INVALID_START.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad message').to.have.property('message').to.be.a('string').to.equal(MESS.LOG.FETCH_ALL.ERROR.MESSAGE[LANG]);
                    expect(res.body.metadata, 'Bad metadata query').to.have.property('query').to.be.a('object');
                    expect(res.body.metadata.query, 'Bad metadata query filter').to.not.have.property('filter');
                    expect(res.body.metadata.query, 'Bad metadata query select').to.have.property('select').to.be.a('array').to.have.length(2);
                    expect(res.body.metadata.query, 'Bad metadata query sort').to.have.property('sort').to.be.a('array').to.have.length(1);
                    expect(res.body.metadata.query, 'Bad metadata query limit').to.not.have.property('limit');
                    expect(res.body.metadata.query, 'Bad metadata query range').to.have.property('range').to.be.a('object');
                    expect(res.body.metadata.query.range, 'Bad metadata query range start').to.have.property('start').to.equal(20);
                    expect(res.body.metadata.query.range, 'Bad metadata query range end').to.have.property('end').to.equal(10);
                    expect(res.body.metadata.query.range, 'Bad metadata query range count').to.have.property('count').to.equal(-9);
                    expect(res.body.metadata, 'Bad metadata links').to.have.property('links').to.be.a('array').to.be.empty;
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.equal(MESS.SYSTEM.QUERY.RANGE_INVALID_START.MESSAGE[LANG]);
                    done();
                });
        });
    });
});
