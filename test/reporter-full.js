'use strict';
var mocha = require('mocha');
var Base = mocha.reporters.Base;
var inherits = mocha.utils.inherits;
var color = Base.color;

exports = module.exports = reporterFull;

function reporterFull(runner) {
  Base.call(this, runner);

  var self = this;
  var indents = 0;
  var n = 0;

  function indent() {
    return Array(indents).join('  ');
  }

  runner.on('start', function() {
    console.log();
  });

  runner.on('suite', function(suite) {
    ++indents;
    console.log(color('suite', '%s%s'), indent(), suite.title);
  });

  runner.on('suite end', function() {
    --indents;
    if (indents === 1) {
      console.log();
    }
  });

  runner.on('pending', function(test) {
    var fmt = indent() + color('pending', '  - %s');
    console.log(fmt, test.title);
  });

  runner.on('pass', function(test) {
    var fmt;
    if (test.speed === 'fast') {
      test.speed = 'green';
    }
    fmt =
      indent() +
      color('checkmark', '  ' + Base.symbols.ok) +
      color('pass', ' %s') +
      color(test.speed, ' (%dms)');
    console.log(fmt, test.title, test.duration);
  });

  runner.on('fail', function(test, err) {
    console.log(indent() + color('fail', '  %d) %s'), ++n, test.title);
  });

  runner.once('end', function(test, err) {
    let durationColor;
    let n = 1;
    if (self.failures.length > 0) {
      console.log('Failures:')
      self.failures.forEach(fail => {
        console.log(
          color('fail', '%d) %s : %s - expected %s, got %s\n'),
          n,
          fail.title,
          fail.err.message.split(':')[0],
          fail.err.expected,
          fail.err.actual
        );
        n++;
      });
    }
    console.log('Results:');
    process.stdout.write('fails : ');
    process.stdout.write(color('fail', self.stats.failures));
    process.stdout.write(' / success : ');
    process.stdout.write(color('green', self.stats.passes));
    process.stdout.write(' / total : ');
    process.stdout.write(color('fast', self.stats.tests));
    if (self.stats.duration < self.stats.tests * 35) {
      durationColor = 'green';
    } else if (self.stats.duration < self.stats.tests * 50) {
      durationColor = 'medium';
    } else {
      durationColor = 'fail';
    }
    process.stdout.write(
      ' - duration : ' + color(durationColor, self.stats.duration) + ' ms\n'
    );
    //self.epilogue.bind(self);
    process.exit();
  });
}

inherits(reporterFull, Base);
