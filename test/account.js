process.env.NODE_ENV = 'testing';
const config = require('config');
const LANG = config.app.LANG.toUpperCase();

const User = require('../models/').User;
const Log = require('../models/').Log;

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const expect = chai.expect;

const MESS = require('../config/messages');

chai.use(chaiHttp);

const baseUrl = '/api/' + config.app.API_VERSION;

const _USER = {
    email: 'test@test.com',
    password: 'P@ssw0rd',
    firstName: 'prénom',
    lastName: 'nom',
    role: 'user'
};
const _USER2 = {
    email: 'test2@test.com',
    password: 'P@ssw0rd2',
    firstName: 'prénom2',
    lastName: 'nom2',
    role: 'user'
};

const INVALID_PASSWORD = 'P@ssw0d';
const UNKNOWN_ID = '11111111-1111-1111-1111-111111111111';
let USER_TOKEN;
let USER_ID;

describe('User Authentification', () => {
    before(async () => {
        await Log.destroy({
            where: {},
        }, err => { });
        await User.destroy({
            where: {},
        }, err => { });
    });

    /*
    * Preparing an user
    */
    describe('/POST register', () => {
        it('[' + MESS.USER.REGISTER.SUCCESS.CODE_HTTP + '] it should register my user ', done => {
            const user = { ..._USER };
            chai
                .request(server)
                .post(baseUrl + '/auth/register')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.REGISTER.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad Response').to.be.empty;
                    done();
                });
        });
        it('[' + MESS.USER.LOGIN.SUCCESS.CODE_HTTP + '] it should login my user', done => {
            const user = { ..._USER };
            chai
                .request(server)
                .post(baseUrl + '/auth/login')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.LOGIN.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(1);
                    expect(res.body.results[0], 'Bad result').to.be.a('object');
                    expect(res.body.results[0], 'Bad result token').to.have.property('token').to.be.a('string');
                    expect(res.body.results[0], 'Bad result expires').to.have.property('expiresIn').to.be.a('number').to.equal(config.user.JWT_EXPIRATIONTIME_HOUR * 60 * 60);
                    expect(res.body.results[0], 'Bad result profile').to.have.property('profile').to.be.a('object');
                    expect(res.body.results[0].profile, 'Bad result profile email').to.have.property('email').to.be.a('string').to.equal(_USER.email);
                    expect(res.body.results[0].profile, 'Bad result profile firstName').to.have.property('firstName').to.be.a('string').to.equal(_USER.firstName);
                    expect(res.body.results[0].profile, 'Bad result profile lastName').to.have.property('lastName').to.be.a('string').to.equal(_USER.lastName);
                    expect(res.body.results[0].profile, 'Bad result profile id').to.have.property('id').to.be.a('string');
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    USER_TOKEN = res.body.results[0].token;
                    USER_ID = res.body.results[0].profile.id;
                    done();
                });
        });
    });

    describe('/GET my account', () => {
        it('[' + MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP + '] it should not get the profile of my user (auth token missing)', done => {
            chai
                .request(server)
                .get(baseUrl + '/my/account/')
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.FETCH_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not get the profile of my user (bad auth token)', done => {
            const token = 'Bearer: 1' + USER_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/my/account/')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.FETCH_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.FETCH_PROFILE.SUCCESS.CODE_HTTP + '] It should get the profile of my user', done => {
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .get(baseUrl + '/my/account')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.FETCH_PROFILE.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.FETCH_PROFILE.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(1);
                    expect(res.body.results[0], 'Bad result').to.be.a('object');
                    expect(res.body.results[0], 'Bad result email').to.have.property('email').to.be.a('string').to.equal(_USER.email);
                    expect(res.body.results[0], 'Bad result firstName').to.have.property('firstName').to.be.a('string').to.equal(_USER.firstName);
                    expect(res.body.results[0], 'Bad result lastName').to.have.property('lastName').to.be.a('string').to.equal(_USER.lastName);
                    expect(res.body.results[0], 'Bad result id').to.have.property('id').to.be.a('string');
                    expect(res.body.results[0], 'Bad result password').to.not.have.property('password');
                    expect(res.body.results[0], 'Bad result role').to.not.have.property('role');
                    expect(res.body.results[0], 'Bad result active').to.not.have.property('active');
                    expect(res.body.results[0], 'Bad result createdAt').to.not.have.property('createdAt');
                    expect(res.body.results[0], 'Bad result updatedAt').to.not.have.property('updatedAt');
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
    });

    describe('/PATCH activate/unactive my account', () => {
        it('[' + MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP + '] it should not desactive my user (auth token missing)', done => {
            chai
                .request(server)
                .patch(baseUrl + '/my/account/unactive')
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not desactive my user (bad auth token)', done => {
            const token = 'Bearer: 1' + USER_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/my/account/unactive')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.UPDATE_PROFILE.SUCCESS.CODE_HTTP + '] it should desactive my user', done => {
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/my/account/unactive')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.UPDATE_PROFILE.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.ACCOUNT_DESACTIVATED.CODE_HTTP + '] it should not login my user (user desactivated)', done => {
            const user = {
                email: _USER.email,
                password: _USER.password
            };
            chai
                .request(server)
                .post(baseUrl + '/auth/login')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.ACCOUNT_DESACTIVATED.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.LOGIN.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.ACCOUNT_DESACTIVATED.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP + '] it should not active my user (auth token missing)', done => {
            chai
                .request(server)
                .patch(baseUrl + '/my/account/active')
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not active my user (bad auth token)', done => {
            const token = 'Bearer: 1' + USER_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/my/account/active')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.UPDATE_PROFILE.SUCCESS.CODE_HTTP + '] it should active my user', done => {
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/my/account/active')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.UPDATE_PROFILE.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.SUCCESS.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.USER.LOGIN.SUCCESS.CODE_HTTP + '] it should login my user (user reactivated)', done => {
            const user = { ..._USER };
            chai
                .request(server)
                .post(baseUrl + '/auth/login')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.LOGIN.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(1);
                    expect(res.body.results[0], 'Bad result').to.be.a('object');
                    expect(res.body.results[0], 'Bad result token').to.have.property('token').to.be.a('string');
                    expect(res.body.results[0], 'Bad result expires').to.have.property('expiresIn').to.be.a('number').to.equal(config.user.JWT_EXPIRATIONTIME_HOUR * 60 * 60);
                    expect(res.body.results[0], 'Bad result profile').to.have.property('profile').to.be.a('object');
                    expect(res.body.results[0].profile, 'Bad result profile email').to.have.property('email').to.be.a('string').to.equal(_USER.email);
                    expect(res.body.results[0].profile, 'Bad result profile firstName').to.have.property('firstName').to.be.a('string').to.equal(_USER.firstName);
                    expect(res.body.results[0].profile, 'Bad result profile lastName').to.have.property('lastName').to.be.a('string').to.equal(_USER.lastName);
                    expect(res.body.results[0].profile, 'Bad result profile id').to.have.property('id').to.be.a('string');
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
    });

    describe('/PUT update user profile', () => {
        it('[' + MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP + '] it should not update the profile of my user (auth token missing)', done => {
            const user = {
                profile: { lastName: _USER2.lastName }
            };
            chai
                .request(server)
                .put(baseUrl + '/my/account/')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not update the profile my user (bad auth token)', done => {
            const user = {
                profile: { lastName: _USER2.lastName }
            };
            const token = 'Bearer: 1' + USER_TOKEN;
            chai
                .request(server)
                .put(baseUrl + '/my/account/')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.MISSING_PROFILE.CODE_HTTP + '] it should not update the the profile with notice (only role in profile)', done => {
            const user = {
                profile: { role: "admin" }
            };
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .put(baseUrl + '/my/account/')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_PROFILE.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_PROFILE.MESSAGE[LANG]);
                    expect(res.body, 'Bad notice').to.have.property('notices').to.be.a('array').to.have.length(1);
                    expect(res.body.notices[0], 'Bad notice message').to.include(MESS.USER.UPDATE_PROFILE.NOTICES.INVALID_FIELD.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.MISSING_PROFILE.CODE_HTTP + '] it should not update the the profile with notice (only active in profile)', done => {
            const user = {
                profile: { active: false }
            };
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .put(baseUrl + '/my/account/')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_PROFILE.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_PROFILE.MESSAGE[LANG]);
                    expect(res.body, 'Bad notice').to.have.property('notices').to.be.a('array').to.have.length(1);
                    expect(res.body.notices[0], 'Bad notice message').to.include(MESS.USER.UPDATE_PROFILE.NOTICES.INVALID_FIELD.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.UPDATE_PROFILE.SUCCESS.CODE_HTTP + '] it should update the profile of my user with notice (bad profile)', done => {
            const user = {
                profile: { lastName: _USER2.lastName, test: 'test' }
            };
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .put(baseUrl + '/my/account/')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.UPDATE_PROFILE.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.SUCCESS.MESSAGE[LANG]);
                    expect(res.body.results[0], 'Bad result').to.be.a('object');
                    expect(res.body.results[0], 'Bad result email').to.have.property('email').to.be.a('string').to.equal(_USER.email);
                    expect(res.body.results[0], 'Bad result firstName').to.have.property('firstName').to.be.a('string').to.equal(_USER.firstName);
                    expect(res.body.results[0], 'Bad result lastName').to.have.property('lastName').to.be.a('string').to.equal(_USER2.lastName);
                    expect(res.body.results[0], 'Bad result id').to.have.property('id').to.be.a('string');
                    expect(res.body.results[0], 'Bad result password').to.not.have.property('password');
                    expect(res.body.results[0], 'Bad result role').to.not.have.property('role');
                    expect(res.body.results[0], 'Bad result active').to.not.have.property('active');
                    expect(res.body.results[0], 'Bad result createdAt').to.not.have.property('createdAt');
                    expect(res.body.results[0], 'Bad result updatedAt').to.not.have.property('updatedAt');
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    expect(res.body, 'Bad notice').to.have.property('notices').to.be.a('array').to.have.length(1);
                    done();
                });
        });
        it('[' + MESS.USER.UPDATE_PROFILE.SUCCESS.CODE_HTTP + '] it should update the profile of my user (lastName & firstName)', done => {
            const user = {
                profile: { lastName: _USER.lastName, firstName: _USER2.firstName }
            };
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .put(baseUrl + '/my/account/')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.UPDATE_PROFILE.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.SUCCESS.MESSAGE[LANG]);
                    expect(res.body.results[0], 'Bad result').to.be.a('object');
                    expect(res.body.results[0], 'Bad result email').to.have.property('email').to.be.a('string').to.equal(_USER.email);
                    expect(res.body.results[0], 'Bad result firstName').to.have.property('firstName').to.be.a('string').to.equal(_USER2.firstName);
                    expect(res.body.results[0], 'Bad result lastName').to.have.property('lastName').to.be.a('string').to.equal(_USER.lastName);
                    expect(res.body.results[0], 'Bad result id').to.have.property('id').to.be.a('string');
                    expect(res.body.results[0], 'Bad result password').to.not.have.property('password');
                    expect(res.body.results[0], 'Bad result role').to.not.have.property('role');
                    expect(res.body.results[0], 'Bad result active').to.not.have.property('active');
                    expect(res.body.results[0], 'Bad result createdAt').to.not.have.property('createdAt');
                    expect(res.body.results[0], 'Bad result updatedAt').to.not.have.property('updatedAt');
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.USER.UPDATE_PROFILE.SUCCESS.CODE_HTTP + '] it should update the profile of my user (firstName)', done => {
            const user = {
                profile: { firstName: _USER.firstName }
            };
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .put(baseUrl + '/my/account/')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.UPDATE_PROFILE.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.SUCCESS.MESSAGE[LANG]);
                    expect(res.body.results[0], 'Bad result').to.be.a('object');
                    expect(res.body.results[0], 'Bad result email').to.have.property('email').to.be.a('string').to.equal(_USER.email);
                    expect(res.body.results[0], 'Bad result firstName').to.have.property('firstName').to.be.a('string').to.equal(_USER.firstName);
                    expect(res.body.results[0], 'Bad result lastName').to.have.property('lastName').to.be.a('string').to.equal(_USER.lastName);
                    expect(res.body.results[0], 'Bad result id').to.have.property('id').to.be.a('string');
                    expect(res.body.results[0], 'Bad result password').to.not.have.property('password');
                    expect(res.body.results[0], 'Bad result role').to.not.have.property('role');
                    expect(res.body.results[0], 'Bad result active').to.not.have.property('active');
                    expect(res.body.results[0], 'Bad result createdAt').to.not.have.property('createdAt');
                    expect(res.body.results[0], 'Bad result updatedAt').to.not.have.property('updatedAt');
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
    });

    describe('/PATCH change user password', () => {
        it('[' + MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP + '] it should not change the password of my user (auth token missing)', done => {
            const user = {
                password: _USER2.password
            };
            chai
                .request(server)
                .patch(baseUrl + '/my/account/change-password')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not change the password of my user (bad auth token)', done => {
            const user = {
                password: _USER2.password
            };
            const token = 'Bearer: 1' + USER_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/my/account/change-password')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.MISSING_PASSWORD.CODE_HTTP + '] it should not change the password of my user (password missing)', done => {
            const user = {
                password: INVALID_PASSWORD
            };
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/my/account/change-password')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_PASSWORD.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_PASSWORD.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_PASSWORD.CODE_HTTP + '] it should not change the password of my user (password too short)', done => {
            const user = {
                password: INVALID_PASSWORD
            };
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/my/account/change-password')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_PASSWORD.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.UPDATE_PROFILE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_PASSWORD.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.UPDATE_PROFILE.SUCCESS.CODE_HTTP + '] it should change the password of my user', done => {
            const user = {
                password: _USER2.password
            };
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .patch(baseUrl + '/my/account/change-password')
                .set('Authorization', token)
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.UPDATE_PROFILE.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_CREDDENTIALS.CODE_HTTP + '] it should not login my user (old password)', done => {
            const user = {
                email: _USER.email,
                password: _USER.password
            };
            chai
                .request(server)
                .post(baseUrl + '/auth/login')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_CREDDENTIALS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.LOGIN.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_CREDDENTIALS.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.LOGIN.SUCCESS.CODE_HTTP + '] it should login my user (with new password)', done => {
            const user = { ..._USER };
            user.password = _USER2.password;
            chai
                .request(server)
                .post(baseUrl + '/auth/login')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.LOGIN.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'Bad results').to.have.property('results').to.be.a('array').to.have.length(1);
                    expect(res.body.results[0], 'Bad result').to.be.a('object');
                    expect(res.body.results[0], 'Bad result token').to.have.property('token').to.be.a('string');
                    expect(res.body.results[0], 'Bad result expires').to.have.property('expiresIn').to.be.a('number').to.equal(config.user.JWT_EXPIRATIONTIME_HOUR * 60 * 60);
                    expect(res.body.results[0], 'Bad result profile').to.have.property('profile').to.be.a('object');
                    expect(res.body.results[0].profile, 'Bad result profile email').to.have.property('email').to.be.a('string').to.equal(_USER.email);
                    expect(res.body.results[0].profile, 'Bad result profile firstName').to.have.property('firstName').to.be.a('string').to.equal(_USER.firstName);
                    expect(res.body.results[0].profile, 'Bad result profile lastName').to.have.property('lastName').to.be.a('string').to.equal(_USER.lastName);
                    expect(res.body.results[0].profile, 'Bad result profile id').to.have.property('id').to.be.a('string');
                    expect(res.body, 'Bad error').to.have.property('error').to.be.null;
                    done();
                });
        });
    });

    describe('/DELETE delete user', () => {
        it('[' + MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP + '] it should not delete my user (auth token missing)', done => {
            chai
                .request(server)
                .delete(baseUrl + '/my/account/')
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.MISSING_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.DELETE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.MISSING_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP + '] it should not delete my user (bad auth token)', done => {
            const token = 'Bearer: 1' + USER_TOKEN;
            chai
                .request(server)
                .delete(baseUrl + '/my/account/')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_TOKEN.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.DELETE.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_TOKEN.MESSAGE[LANG]);
                    done();
                });
        });
        it('[' + MESS.USER.DELETE.SUCCESS.CODE_HTTP + '] it should delete my user', done => {
            const token = 'Bearer: ' + USER_TOKEN;
            chai
                .request(server)
                .delete(baseUrl + '/my/account/')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.DELETE.SUCCESS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.empty;
                    done();
                });
        });
        it('[' + MESS.USER.ERRORS.INVALID_CREDDENTIALS.CODE_HTTP + '] it should not login my user (deleted)', done => {
            const user = {
                email: _USER.email,
                password: _USER.password
            };
            chai
                .request(server)
                .post(baseUrl + '/auth/login')
                .send(user)
                .end((err, res) => {
                    expect(err, 'The test returned an error').to.be.null;
                    expect(res, 'Bad http code').to.have.status(MESS.USER.ERRORS.INVALID_CREDDENTIALS.CODE_HTTP);
                    expect(res.body, 'Bad response').to.be.a('object');
                    expect(res.body, 'bad message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.LOGIN.ERROR.MESSAGE[LANG]);
                    expect(res.body, 'Bad results').to.have.property('results').to.be.null;
                    expect(res.body, 'Bad error').to.have.property('error').to.be.a('object');
                    expect(res.body.error, 'Bad error message').to.have.property('message').to.be.a('string').to.equal(MESS.USER.ERRORS.INVALID_CREDDENTIALS.MESSAGE[LANG]);
                    done();
                });
        });
    });
    
});
