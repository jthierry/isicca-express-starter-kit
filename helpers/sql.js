exports.updateSqlAttributes = (locals, model = null) => {
    const exclude = [];
    if (model === 'User') {
        exclude.push('password');
        if (locals.user.role === 'user') {
            exclude.push('role', 'active');
        }
    }
    if (locals.user.role === 'user') {
        exclude.push('createdAt', 'updatedAt');
    }
    if (!locals.customquery.attributes) {
        return { exclude: exclude };
    }
    const newAttributes = [];
    for (let i = 0; i < locals.customquery.attributes.length; i++) {
        if (!(exclude.includes(locals.customquery.attributes[i]))) {
            newAttributes.push(locals.customquery.attributes[i]);
        }
    }
    return newAttributes;
}