const config = require('config');

exports.isValidMongoId = (id) => {
    regex = /^[a-f\d]{24}$/i;
    if (regex.test(id)) {
        return true;
    }
    return false;
}

exports.isValidUuid = (id) => {
    regex = /^[a-f\d]{8}\-[a-f\d]{4}\-[a-f\d]{4}\-[a-f\d]{4}\-[a-f\d]{12}$/i;
    if (regex.test(id)) {
        return true;
    }
    return false;
}

exports.isValidPassword = (password) => {
    const passwordRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{" + config.user.MIN_SIZE_PASSWORD + ",})");
    if (passwordRegex.test(String(password))) {
        return true;
    }
    return false;
}

exports.isValidEmail = (email) => {
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (emailRegex.test(String(email).toLowerCase())) {
        return true;
    }
    return false;
}

exports.isOddNumber = (number) => {
    return number % 2;
}
