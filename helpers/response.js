const config = require('config');
const LANG = config.app.LANG.toUpperCase();
const MESS = require('../config/messages');

const Log = require('../models/').Log;

saveLog = async (req, res, err = null) => {
  if (req.body.password) {
    delete req.body.password;
  }
  const log = {
    req: JSON.stringify({
      query: req.query,
      params: req.params,
      body: req.body,
      headers: req.headers
    }),
    res: JSON.stringify({
      headers: res.getHeaders(),
      httpVersion: req.httpVersion
    }),
    action: req.locals.message ? req.locals.message.ACTION : null,
    success: res.statusCode.toString().charAt(0) === '2' ? true : false,
    reqMethod: req.method,
    reqUrl: req.originalUrl,
    reqUserAgent: req.header('user-agent'),
    reqIp: req.header('x-forwarded-for') || req.connection.remoteAddress,
    resHttpCode: res.statusCode,
    resTime: calculateResTime(req._startAt, res._startAt),
    error: null,
    notices: null
  };
  if (err) {
    if (err.name) {
      log.error = JSON.stringify({
        internal: true,
        name: err.name,
        message: err.message,
        stack: err.stack
      });
    } else {
      log.error = JSON.stringify({
        internal: false,
        message: err.MESSAGE
      });
    }
  }
  if (res.locals.notices.length > 1) {
    log.notices = res.locals.notices.toString();
  }
  result = await Log.create(log);
  if (req.locals.user) {
    result.setUser([req.locals.user.id]);
  }
  return res;
};

buildResJSON = (req, res, datas) => {
  const message = (datas.error) ? req.locals.message.ERROR : req.locals.message.SUCCESS;
  let json = {};
  if (res.locals.httpCode === 201 || res.locals.httpCode === 204 || res.locals.httpCode === 404) {
    return;
  }
  json.message = message.MESSAGE[LANG];
  if (config.app.METADATA) {
    json.metadata = {
      url: req.baseUrl + req._parsedUrl.pathname,
      query: res.locals.customquery,
      links: res.locals.links,
      error: (datas.error) ? 1 : 0,
      notices: res.locals.notices.length,
      results: (datas.results) ? datas.results.length : 0
    };
  }
  json.results = datas.results;
  json.error = datas.error;
  json.notices = (res.locals.notices.length > 0) ? res.locals.notices : null;
  return json;
}

exports.success = (req, res, next, results = null) => {
  res.locals.httpCode = req.locals.message.SUCCESS.CODE_HTTP;
  results = processResults(req, res, next, results);
  if (results !== false) {
    res.status(res.locals.httpCode).json(buildResJSON(req, res, { results: results, error: null }));
    if (config.log.DB === true) {
      saveLog(req, res);
    }
  }
};

exports.notice = (req, res, next, notice, error) => {
  res.locals.notices.push(notice.MESSAGE[LANG] + ' : ' + error);
};

exports.error = (req, res, next, err) => {
  processError(req, res, next, err);
};

processError = (req, res, next, err) => {
  res.locals.httpCode = err.CODE_HTTP || 500;
  const error = (err.MESSAGE) ? { message: err.MESSAGE[LANG] } : { message: 'Internal error, please contact administrator' };
  res.status(res.locals.httpCode).json(buildResJSON(req, res, { results: null, error: error }));
  if (!err.CODE_HTTP) {
    console.log(err);
  }
  if (config.log.DB === true) {
    saveLog(req, res, err);
  }
};

processResults = (req, res, next, results) => { 
  if (results) {
    if (results.length === undefined) {
      results = [results];
    }
    if (res.locals.customquery.range) {
      if (res.locals.customquery.range.count > results.length) {
        res.locals.customquery.range.count = results.length;
        res.locals.customquery.range.end = (res.locals.customquery.range.start + res.locals.customquery.range.count - 1);
      }
      processNavLinks(req, res, next, results);
      results = processRangeResults(req, res, next, results);
    }
  }
  return results;
}

processRangeResults = (req, res, next, results) => {
  if (res.locals.customquery.range.start > results.length) {
    res.setHeader('Content-Range', '*/' + results.length);
    results = false;
    processError(req, res, next, MESS.SYSTEM.QUERY.RANGE_INVALID_VALUE);
  } else {
    res.setHeader('Content-Range', res.locals.customquery.range.start + '-' + ((res.locals.customquery.range.end > results.length) ? results.length - 1 : res.locals.customquery.range.end) + '/' + results.length);
    if (res.locals.customquery.range.count < results.length) {
      res.locals.httpCode = 206;
    }
    results = results.slice(res.locals.customquery.range.start, res.locals.customquery.range.end + 1);
  }
  return results;
}

processNavLinks = (req, res, next, results) => {
  let query = '';
  if (req._parsedUrl.search !== null) {
    fields = req._parsedUrl.href.substring(2).split('&');
    for (property in fields) {
      if (fields[property].substring(0, 6).toLowerCase() !== 'range=') {
        query = query + fields[property] + '&';
      }
    }
  }
  url = req.baseUrl + req._parsedUrl.pathname + '?' + query;
  res.locals.links.push({ "rel": "first", "href": url + 'range=0,' + (res.locals.customquery.range.count - 1), "method": req.method.toUpperCase() });
  if (res.locals.customquery.range.start <= results.length) {
    if (res.locals.customquery.range.start - res.locals.customquery.range.count - 1 > 0) {
      res.locals.links.push({ "rel": "previous", "href": url + 'range=' + (res.locals.customquery.range.start - res.locals.customquery.range.count) + ',' + (res.locals.customquery.range.start - 1), "method": req.method.toUpperCase() });
    } else if (res.locals.customquery.range.start > 0) {
      res.locals.links.push({ "rel": "previous", "href": url + 'range=' + 0 + ',' + (res.locals.customquery.range.start), "method": req.method.toUpperCase() });
    }
  }
  if ((res.locals.customquery.range.end + 1) < (results.length - 1)) {
    res.locals.links.push({ "rel": "next", "href": url + 'range=' + (res.locals.customquery.range.end + 1) + ',' + (res.locals.customquery.range.end + res.locals.customquery.range.count), "method": req.method.toUpperCase() });
  }
  res.locals.links.push({ "rel": "last", "href": url + 'range=' + (results.length - res.locals.customquery.range.count) + ',' + (results.length - 1), "method": req.method.toUpperCase() });
};

calculateResTime = (req, res) => {
  if (req && res) {
    /* istanbul ignore next */
    return ((res[0] - req[0]) * 1e3 + (res[1] - req[1]) * 1e-6).toFixed(3);
  }
  return 0;
};
