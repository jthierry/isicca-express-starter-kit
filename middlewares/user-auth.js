const config = require('config');
const jwt = require('jsonwebtoken');

const MESS = require('../config/messages');

// need authorization token : Authorisation: "Bearer: token"
exports.isAuth = (req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw MESS.USER.ERRORS.MISSING_TOKEN;
    }
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.verify(token, config.user.JWT_KEY);
    req.locals.user = {
      email: decodedToken.email,
      id: decodedToken.id,
      role: decodedToken.role
    };
    req.locals.id = decodedToken.id;
    next();
  } catch (err) {
    if (err.name === 'JsonWebTokenError') {
      err = MESS.USER.ERRORS.INVALID_TOKEN;
    } else if (err.name === 'TokenExpiredError') {
      err = MESS.USER.ERRORS.EXPIRED_TOKEN;
    }
    if (!res.locals.error) {
      res.locals.error = err;
    }
    next();
  }
};

exports.isAdmin = (req, res, next) => {
  try {
    if (req.locals.user.role === 'admin') {
      next();
    } else {
      throw MESS.USER.ERRORS.NOT_ADMIN;
    }
  } catch (err) {
    if (!res.locals.error) {
      res.locals.error = err;
    }
    next();
  }
};

exports.logUser = (req, res, next) => {
  try {
    req.locals.customquery.where = res.locals.customquery.filter = { user: req.locals.user.id };
    next();
  } catch (err) {
    if (!res.locals.error) {
      res.locals.error = err;
    }
    next();
  }
}