const config = require('config');

const formatHlp = require('../helpers/format');
const MESS = require('../config/messages');

exports.process = model => {
    return (req, res, next) => {
      try {
        Model = require('../models')[model];
        res.setHeader('Accept-Range', model + ' ' + config.query.RANGE_MAX);
        if (req.query.filter) {
          processQueryFilter(req, res, next, Model);
        }
        if (!req.query.sort) {
          req.query.sort = config.query.SORT_DEFAULT;
        }
        processQuerySort(req, res, next, Model);
        if (req.query.limit) {
          processQueryLimit(req, res, next, Model);
        }
        if (req.query.select) {
          processQuerySelect(req, res, next, Model);
        }
        if (!req.query.range) {
          req.query.range = '0,' + config.query.RANGE_MAX;
        }
        processQueryRange(req, res, next, Model);
        next();
      } catch (err) {
        if (!res.locals.error) {
          res.locals.error = err;
        }
        next();
      }
    };
  };
  
  processQueryFilter = (req, res, next, Model) => {
    let fields = [];
    if (req.query.filter.indexOf(',') === -1) {
      fields.push(req.query.filter);
    } else {
      fields = req.query.filter.split(',');
    }
    const regex = /\(.*\)/;
    if (typeof req.locals.customquery.where === 'undefined') {
      req.locals.customquery.where = {};
    }
    for (let i = 0; i < fields.length; i++) {
      if (!regex.test(fields[i])) {
        res.locals.notices.push(MESS.SYSTEM.QUERY.FILTER_INVALID_FIELD_FORMAT.MESSAGE[config.app.LANG.toUpperCase()] + ' : ' + fields[i]);
        break;
      }
      const field = fields[i].split('(');
      let foreignKey = false;
      let foreignModel;
      if (Model.name.toLowerCase() + '_' + field[0] in Model.options.sequelize.models || field[0] + '_' + Model.name.toLowerCase() in Model.options.sequelize.models) {
        foreignKey = true;
        foreignModel = require('../models')[formatHlp.capitalizeFirstLetter(field[0])];
        req.locals.customquery.includeModels.push(formatHlp.capitalizeFirstLetter(field[0]));
      }
      if (!(Model.rawAttributes[field[0]]) && foreignKey === false) {
        res.locals.notices.push(MESS.SYSTEM.QUERY.FILTER_INVALID_FIELD.MESSAGE[config.app.LANG.toUpperCase()] + ' : ' + field[0]);
        break;
      }
      const value = field[1].substring(0, field[1].length - 1);
      if (value.indexOf('|') === -1) {
        switch (value.charAt()) {
          case '<':
            if (foreignKey === true) {
              req.locals.customquery.include.push({
                model: foreignModel,
                as: field[0] + 's',
                attributes: [],
                through: { attributes: [] },
                where: { id: { '$lt': value } }
              });
            } else {
              req.locals.customquery.where[field[0]] = { '$lt': value.substring(1) };
            }
            break;
          case '>':
            if (foreignKey === true) {
              req.locals.customquery.include.push({
                model: foreignModel,
                as: field[0] + 's',
                attributes: [],
                through: { attributes: [] },
                where: { id: { '$gt': value } }
              });
            } else {
              req.locals.customquery.where[field[0]] = { '$gt': value.substring(1) };
            }
            break;
          default:
            if (foreignKey === true) {
              req.locals.customquery.include.push({
                model: foreignModel,
                as: field[0] + 's',
                attributes: [],
                through: { attributes: [] },
                where: { id: value }
              });
            } else {
              req.locals.customquery.where[field[0]] = { '$eq': value };
            }
            break;
        }
      } else {
        const vals = value.split('|');
  
        if (foreignKey) {
          req.locals.customquery.include.push({
            model: foreignModel,
            as: field[0] + 's',
            attributes: [],
            through: { attributes: [] },
            where: { id: {} }
          });
          const inc = [];
          for (val in vals) {
            switch (vals[val].charAt()) {
              case '<':
                if ('$lt' in req.locals.customquery.include[req.locals.customquery.include.length - 1].where.id) {
                  res.locals.notices.push(MESS.SYSTEM.QUERY.FILTER_INVALID_VALUE_LT.MESSAGE[config.app.LANG.toUpperCase()] + ' : ' + field[0]);
                  break;
                }
                req.locals.customquery.include[req.locals.customquery.include.length - 1].where.id['$lt'] = vals[val].substring(1);
                break;
              case '>':
                if ('$gt' in req.locals.customquery.include[req.locals.customquery.include.length - 1].where.id) {
                  res.locals.notices.push(MESS.SYSTEM.QUERY.FILTER_INVALID_VALUE_GT.MESSAGE[config.app.LANG.toUpperCase()] + ' : ' + field[0]);
                  break;
                }
                req.locals.customquery.include[req.locals.customquery.include.length - 1].where.id['$gt'] = vals[val].substring(1);
                break;
              default:
                if ('$lt' in req.locals.customquery.include[req.locals.customquery.include.length - 1].where.id || '$gt' in req.locals.customquery.include[req.locals.customquery.include.length - 1].where.id) {
                  res.locals.notices.push(MESS.SYSTEM.QUERY.FILTER_INVALID_VALUE_EQ.MESSAGE[config.app.LANG.toUpperCase()] + ' : ' + field[0]);
                  break;
                }
                inc.push(vals[val]);
                break;
            }
            if (inc.length > 0) {
              req.locals.customquery.include[req.locals.customquery.include.length - 1].where.id = { '&in': inc }
            }
          }
        } else {
          req.locals.customquery.where[field[0]] = {};
          const eq = [];
          for (val in vals) {
            switch (vals[val].charAt()) {
              case '<':
                if ('$lt' in req.locals.customquery.where[field[0]]) {
                  res.locals.notices.push(MESS.SYSTEM.QUERY.FILTER_INVALID_VALUE_LT.MESSAGE[config.app.LANG.toUpperCase()] + ' : ' + field[0]);
                  break;
                }
                req.locals.customquery.where[field[0]]['$lt'] = vals[val].substring(1);
                break;
              case '>':
                if ('$gt' in req.locals.customquery.where[field[0]]) {
                  res.locals.notices.push(MESS.SYSTEM.QUERY.FILTER_INVALID_VALUE_GT.MESSAGE[config.app.LANG.toUpperCase()] + ' : ' + field[0]);
                  break;
                }
                req.locals.customquery.where[field[0]]['$gt'] = vals[val].substring(1);
                break;
              default:
                if ('$lt' in req.locals.customquery.where[field[0]] || '$gt' in req.locals.customquery.where[field[0]]) {
                  res.locals.notices.push(MESS.SYSTEM.QUERY.FILTER_INVALID_VALUE_EQ.MESSAGE[config.app.LANG.toUpperCase()] + ' : ' + field[0]);
                  break;
                }
                eq.push(vals[val]);
                break;
            }
            if (eq.length > 0) {
              req.locals.customquery.where[field[0]]['$in'] = eq;
            }
          }
        }
      }
    }
    res.locals.customquery.filter = { ...req.locals.customquery.where, ...req.locals.customquery.include };
  };
  
  processQuerySelect = (req, res, next, Model) => {
    const regex = /\(.*\)/;
    if (!regex.test(req.query.select)) {
      res.locals.notices.push(MESS.SYSTEM.QUERY.SELECT_INVALID_FIELD_FORMAT.MESSAGE[config.app.LANG.toUpperCase()] + ' : ' + req.query.select);
      return;
    }
    const fields = req.query.select.split('(');
    let select = ['id'];
    const values = fields[1].substring(0, fields[1].length - 1).split(',');
    for (let i = 0; i < values.length; i++) {
      if (!Model.rawAttributes[values[i]]) {
        res.locals.notices.push(MESS.SYSTEM.QUERY.SELECT_INVALID_FIELD.MESSAGE[config.app.LANG.toUpperCase()] + ' : ' + values[i]);
        break;
      }
      if (values[i] !== 'id') {
        select.push(values[i]);
      }
    }
    if (fields[0].toLowerCase() === 'include') {
      req.locals.customquery.attributes = select;
    } else if (fields[0].toLowerCase() === 'exclude') {
      select = select.slice(1);
      req.locals.customquery.attributes = { exclude: select };
    } else {
      res.locals.notices.push(MESS.SYSTEM.QUERY.SELECT_INVALID_OPERATOR.MESSAGE[config.app.LANG.toUpperCase()] + ' : ' + fields[0]);
      return;
    }
    res.locals.customquery.select = req.locals.customquery.attributes;
  };
  
  processQuerySort = (req, res, next, Model) => {
    let fields = [];
    if (req.query.sort.indexOf(',') === -1) {
      fields.push(req.query.sort);
    } else {
      fields = req.query.sort.split(',');
    }
    const regex = /\(.*\)/;
    req.locals.customquery.order = [];
    for (let i = 0; i < fields.length; i++) {
      if (!regex.test(fields[i])) {
        res.locals.notices.push(MESS.SYSTEM.QUERY.SORT_INVALID_FIELD_FORMAT.MESSAGE[config.app.LANG.toUpperCase()] + ' : ' + fields[i]);
        break;
      }
      const field = fields[i].split('(');
      if (!(Model.rawAttributes[field[0]])) {
        res.locals.notices.push(MESS.SYSTEM.QUERY.SORT_INVALID_FIELD.MESSAGE[config.app.LANG.toUpperCase()] + ' : ' + field[0]);
        break;
      }
      const value = field[1].substring(0, field[1].length - 1).toLowerCase();
      if (value !== 'asc' && value !== 'desc') {
        res.locals.notices.push(MESS.SYSTEM.QUERY.SORT_INVALID_VALUE.MESSAGE[config.app.LANG.toUpperCase()] + ' : ' + value);
        break;
      }
      req.locals.customquery.order.push([field[0], value.toUpperCase()]);
    }
    res.locals.customquery.sort = req.locals.customquery.order;
  };
  
  processQueryLimit = (req, res, next, Model) => {
    limit = Number(req.query.limit);
    if (!Number.isInteger(limit)) {
      res.locals.notices.push(MESS.SYSTEM.QUERY.LIMIT_INVALID_VALUE.MESSAGE[config.app.LANG.toUpperCase()] + ' : ' + req.query.limit);
      return;
    }
    req.locals.customquery.limit = limit;
    res.locals.customquery.limit = limit;
  };
  
  processQueryRange = (req, res, next, Model) => {
    if (req.query.range.indexOf(',') === -1) {
      res.locals.notices.push(MESS.SYSTEM.QUERY.RANGE_INVALID_FIELD_FORMAT.MESSAGE[config.app.LANG.toUpperCase()] + ' : ' + req.query.range);
      return;
    }
    fields = req.query.range.split(',');
    let start = Number(fields[0]);
    let end = Number(fields[1]);
    if (fields.length !== 2 || !Number.isInteger(start) || !Number.isInteger(end)) {
      res.locals.notices.push(MESS.SYSTEM.QUERY.RANGE_INVALID_FIELD_FORMAT.MESSAGE[config.app.LANG.toUpperCase()] + ' : ' + req.query.range);
      start = 0;
      end = config.query.RANGE_MAX;
    }
    if (end < start) {
      res.locals.error = MESS.SYSTEM.QUERY.RANGE_INVALID_START;
    }
    if (end - start > config.query.RANGE_MAX) {
      res.locals.notices.push(MESS.SYSTEM.QUERY.RANGE_TOO_BIG.MESSAGE[config.app.LANG.toUpperCase()] + ' : ' + (end - start));
      start = 0;
      end = config.query.RANGE_MAX;
    }
    req.locals.customquery.range = { start: start, end: end, count: (end - start + 1) };
    res.locals.customquery.range = req.locals.customquery.range;
  };
  