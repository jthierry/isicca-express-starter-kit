const responseHlp = require('../helpers/response');

exports.populate = message => {
  return (req, res, next) => {
    req.locals.message = message;
    if (res.locals.error) {
      responseHlp.error(req, res, next, res.locals.error);
    } else {
      next();
    }
  };
};

