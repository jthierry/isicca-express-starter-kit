const config = require('config');

const formatHlp = require('../helpers/format');
const MESS = require('../config/messages');

exports.isEmail = (req, res, next) => {
  try {
    if (!req.body.email) {
      throw MESS.USER.ERRORS.MISSING_EMAIL;
    }
    if (!formatHlp.isValidEmail(req.body.email)) {
      throw MESS.USER.ERRORS.INVALID_EMAIL;
    }
    req.locals.profile.email = req.body.email;
    next();
  } catch (err) {
    if (!res.locals.error) {
      res.locals.error = err;
    }
    next();
  }
};

exports.isPassword = (req, res, next) => {
  try {
    if (!req.body.password) {
      throw MESS.USER.ERRORS.MISSING_PASSWORD;
    }
    if (!formatHlp.isValidPassword(req.body.password)) {
      throw MESS.USER.ERRORS.INVALID_PASSWORD;
    }
    req.locals.profile.password = req.body.password;
    next();
  } catch (err) {
    if (!res.locals.error) {
      res.locals.error = err;
    }
    next();
  }
};

exports.isUserCreationFields = (req, res, next) => {
  try {
    if (config.user.ALLOW_CREATION_SELFADMIN && req.body.role && req.body.role === 'admin') {
      req.locals.profile.role = req.body.role;
    }
    if (req.body.firstName) {
      req.locals.profile.firstName = req.body.firstName;
    }
    if (req.body.lastName) {
      req.locals.profile.lastName = req.body.lastName;
    }
    next();
  } catch (err) {
    if (!res.locals.error) {
      res.locals.error = err;
    }
    next();
  }
}

exports.isDbId = (req, res, next) => {
  try {
    if (!req.params.id) {
      throw MESS.SYSTEM.DB.ERRORS.MISSING_ID;
    }
    if (!formatHlp.isValidUuid(req.params.id)) {
      throw MESS.SYSTEM.DB.ERRORS.INVALID_ID;
    }
    req.locals.id = req.params.id;
    next();
  } catch (err) {
    if (!res.locals.error) {
      res.locals.error = err;
    }
    next();
  }
}

exports.isUserId = (req, res, next) => {
  try {
    if (!req.body.userId) {
      throw MESS.USER.ERRORS.MISSING_ID;
    }
    if (!formatHlp.isValidUuid(req.body.userId)) {
      throw MESS.SYSTEM.DB.ERRORS.INVALID_ID;
    }
    req.locals.id = req.body.userId;
    next();
  } catch (err) {
    if (!res.locals.error) {
      res.locals.error = err;
    }
    next();
  }
};
