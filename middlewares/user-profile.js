const responseHlp = require('../helpers/response');
const MESS = require('../config/messages');
const Model = require('../models/').User;

exports.updateActiveState = active => {
  return (req, res, next) => {
    try {
      req.locals.profile.activeState = { active: active };
      next();
    } catch (err) {
      if (!res.locals.error) {
        res.locals.error = err;
      }
      next();
    }
  };
};

exports.updateProfile = (req, res, next) => {
  try {
    if (!req.body.profile) {
      throw MESS.USER.ERRORS.MISSING_PROFILE;
    }
    if (typeof req.body.profile === 'string') {
      req.body.profile = JSON.parse(req.body.profile);
    }
    for (let property in req.body.profile) {
      if (!(Model.rawAttributes[property]) || property === 'role' || property === 'active') {
        responseHlp.notice(req, res, next, MESS.USER.UPDATE_PROFILE.NOTICES.INVALID_FIELD, property);
      } else {
        req.locals.profile[property] = req.body.profile[property];
      }
    }
    if (Object.keys(req.locals.profile).length === 0){
      throw MESS.USER.ERRORS.MISSING_PROFILE;
    }
    next();
  } catch (err) {
    if (!res.locals.error) {
      res.locals.error = err;
    }
    next();
  }
};

exports.updateRole = role => {
  return (req, res, next) => {
    try {
      req.locals.profile.role = { role: role };
      next();
    } catch (err) {
      if (!res.locals.error) {
        res.locals.error = err;
      }
      next();
    }
  };
};
