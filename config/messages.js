exports.SYSTEM = {
    DB: {
        ERRORS: {
            CONNEXION: {
                MESSAGE: {
                    EN: "Unable to connect to the database",
                    FR: "Impossible de se connecter à la base de données",
                },
                CODE_HTTP: 500,
            },
            MISSING_ID: {
                MESSAGE: {
                    EN: "The id is missing",
                    FR: "L'id est manquant",
                },
                CODE_HTTP: 400,
            },
            INVALID_ID: {
                MESSAGE: {
                    EN: "The given id is invalid",
                    FR: "L'id fourni est invalide",
                },
                CODE_HTTP: 422,
            },
        },
    },
    QUERY: {
        RANGE_INVALID_START: {
            MESSAGE: {
                EN: "The range function has invalid values (start value is bigger than the end value)",
                FR: "La fonction de pagination a une valeur invalide (la valeur de début est plus grande que la valeur de fin)",
            },
            CODE_HTTP: 416,
        },
        RANGE_INVALID_VALUE: {
            MESSAGE: {
                EN: "The range function has invalid values (start value is bigger than the total of results)",
                FR: "La fonction de pagination a une valeur invalide (la valeur de début est plus grande que le total de résultats)",
            },
            CODE_HTTP: 416,
        },
        RANGE_TOO_BIG: {
            MESSAGE: {
                EN: "The range function has invalid values (max range value: 50 results)",
                FR: "La fonction de pagination a une valeur invalide (valeur maximum: 50 résultats)",
            },
            CODE_HTTP: 416,
        },
        RANGE_INVALID_FIELD_FORMAT: {
            MESSAGE: {
                EN: "The range function has an invalid format (need range=start,end)",
                FR: "La fonction de pagination a un format invalide (doit être range=start,end)",
            },
            CODE_HTTP: 400,
        },
        FILTER_INVALID_FIELD_FORMAT: {
            MESSAGE: {
                EN: "The filter function has an invalid format (need filter=field1,value1+value2,field2,value1|value2,fieldN,valueN )",
                FR: "La fonction de filtre a un format invalide (doit être filter=champs1,valeur1+value2,champs2,valeur1|valeur2,champsN,valeurN)",
            },
            CODE_HTTP: 400,
        },
        FILTER_INVALID_FIELD: {
            MESSAGE: {
                EN: "The given field for the filter function does not exist",
                FR: "Le champs spécifié dans la fonction de filtre n'existe pas",
            },
            CODE_HTTP: 422,
        },
        FILTER_INVALID_VALUE_GT: {
            MESSAGE: {
                EN: "The field cannot have more than one greater than (>) operator",
                FR: "La champs ne peut avoir plus d'un opérateur plus grand que (>)",
            },
            CODE_HTTP: 422,
        },
        FILTER_INVALID_VALUE_LT: {
            MESSAGE: {
                EN: "The field cannot have more than one lesser than (<) operator",
                FR: "La champs ne peut avoir plus d'un opérateur plus petit que (<)",
            },
            CODE_HTTP: 422,
        },
        FILTER_INVALID_VALUE_EQ: {
            MESSAGE: {
                EN: "The field cannot have both comparaison and equal values",
                FR: "La champs ne peut avoir à la fois de comparaison et d'égalité",
            },
            CODE_HTTP: 422,
        },
        SELECT_INVALID_FIELD_FORMAT: {
            MESSAGE: {
                EN: "The select function has an invalid format (need select(field1,field2,fieldN))",
                FR: "La fonction de sélection a un format invalide (doit être select(champs1,champs2,champsN))",
            },
            CODE_HTTP: 400,
        },
        SELECT_INVALID_OPERATOR: {
            MESSAGE: {
                EN: "The select function has an invalid operator (need select or unselect)",
                FR: "La fonction de sélection a un opérateur invalide (doit être select ou unselect)",
            },
            CODE_HTTP: 400,
        },
        SELECT_INVALID_FIELD: {
            MESSAGE: {
                EN: "One of the given fields for the fields select function does not exist",
                FR: "Un des champs spécifiés dans la fonction de sélection de champs n'existe pas",
            },
            CODE_HTTP: 422,
        },
        SORT_INVALID_FIELD_FORMAT: {
            MESSAGE: {
                EN: "The sort function has an invalid format (need sort=field1,value1,fieldN,valueN)",
                FR: "La fonction de tri a un format invalide (doit être sort=champs1,valeur1,champsN,valeurN)",
            },
            CODE_HTTP: 400,
        },
        SORT_INVALID_FIELD: {
            MESSAGE: {
                EN: "The given field for the sort function does not exist and has been ignored",
                FR: "Le champs spécifié dans la fonction de tri n'existe pas et a été ignoré",
            },
            CODE_HTTP: 422,
        },
        SORT_INVALID_VALUE: {
            MESSAGE: {
                EN: "The given value for the sort function is not valid (asc or desc needed) and has been ignored",
                FR: "La valeur spécifiée dans la fonction de tri n'est pas conforme (asc ou desc requis) et a été ignorée",
            },
            CODE_HTTP: 422,
        },
        LIMIT_INVALID_VALUE: {
            MESSAGE: {
                EN: "The given value for the limit function is not an interger number and has been ignored",
                FR: "La valeur spécifiée dans la fonction de limitation n'est pas un nombre entier et a été ignorée",
            },
            CODE_HTTP: 422,
        }
    },
    ERRORS: {
        LOG_INSUFFISANT_RIGHTS: {
            MESSAGE: {
                EN: "Unable to create the log folder. Check your folder permissions",
                FR: "Impossible de créer le dossier de logs. Vérifiez les permissions du dossier",
            },
            CODE_HTTP: 500,
        },
        PAGE_NOT_FOUND: {
            MESSAGE: {
                EN: "Endpoint not found",
                FR: "Point d'accès non trouvé!",
            },
            CODE_HTTP: 404,
        },
    },
};

exports.LOG = {
    ERRORS: {
        MISSING_ID: {
            MESSAGE: {
                EN: "The log id is missing",
                FR: "L'id du log est manquant",
            },
            CODE_HTTP: 400,
        },
        INVALID_ID: {
            MESSAGE: {
                EN: "The log id is invalid",
                FR: "L'id du log est invalide",
            },
            CODE_HTTP: 422,
        },
        INVALID_TYPE: {
            MESSAGE: {
                EN: "The log type is invalid",
                FR: "Le type du log est invalide",
            },
            CODE_HTTP: 422,
        },
    },
    FETCH_ALL: {
        ACTION: "Retrieve logs",
        SUCCESS: {
            MESSAGE: {
                EN: "Logs retrieved with success",
                FR: "Logs récupérés avec succès",
            },
            CODE_HTTP: 200,
        },
        ERROR: {
            MESSAGE: {
                EN: "Failed to retrieve logs",
                FR: "Echec lors de la récuperation des logs",
            },
            CODE_HTTP: 500,
        },
    },
    FETCH_ONE: {
        ACTION: "Retrieve a specific log",
        SUCCESS: {
            MESSAGE: {
                EN: "Log retrieved with success",
                FR: "Log récupéré avec success",
            },
            CODE_HTTP: 200,
        },
        ERROR: {
            MESSAGE: {
                EN: "Failed to retrieve the log",
                FR: "Echec lors de la récuperation du log",
            },
            CODE_HTTP: 500,
        },
    },
};

exports.ADMIN = {
    ERRORS: {
        INVALID_USER: {
            MESSAGE: {
                EN: "The given user does not exist",
                FR: "L'utilisateur fourni n'existe pas",
            },
            CODE_HTTP: 422,
        },
    },
    FETCH_ALL: {
        ACTION: "Retrieve users",
        SUCCESS: {
            MESSAGE: {
                EN: "Users retrieved with success",
                FR: "Utilisateurs récupérés avec succès",
            },
            CODE_HTTP: 200,
        },
        ERROR: {
            MESSAGE: {
                EN: "Failed to retrieve users",
                FR: "Echec lors de la récuperation des utilisateurs",
            },
            CODE_HTTP: 500,
        },
    },
    DELETE_ONE: {
        ACTION: "Delete a specific user",
        SUCCESS: {
            MESSAGE: {
                EN: "User deleted with success",
                FR: "utilisateur supprimé avec succès",
            },
            CODE_HTTP: 204,
        },
        ERROR: {
            MESSAGE: {
                EN: "Failed to delete the user",
                FR: "Echec lors de la suppression de l'utilisateur",
            },
            CODE_HTTP: 500,
        },
    },
};

exports.USER = {
    ERRORS: {
        MISSING_EMAIL: {
            MESSAGE: {
                EN: "The email is missing",
                FR: "L'email est manquant",
            },
            CODE_HTTP: 400,
        },
        INVALID_EMAIL: {
            MESSAGE: {
                EN: "The email is invalid",
                FR: "L'email fourni est invalide",
            },
            CODE_HTTP: 422,
        },
        EXISTING_EMAIL: {
            MESSAGE: {
                EN: "Email already in use",
                FR: "Email déjà utilisé",
            },
            CODE_HTTP: 409,
        },
        MISSING_PASSWORD: {
            MESSAGE: {
                EN: "The password is missing",
                FR: "Le mot de passe est manquant",
            },
            CODE_HTTP: 400,
        },
        INVALID_PASSWORD: {
            MESSAGE: {
                EN: "The password is invalid (8 characters minimum including 1 min, 1 maj, 1 number, 1 special)",
                FR: "Le mot de passe fourni est invalide (8 caractères min incluant 1min, 1 maj, 1 chiffre et 1 spécial)",
            },
            CODE_HTTP: 422,
        },
        INVALID_TOKEN: {
            MESSAGE: {
                EN: "The identification token is invalid",
                FR: "Le token d'identification fourni est invalide",
            },
            CODE_HTTP: 422,
        },
        MISSING_TOKEN: {
            MESSAGE: {
                EN: "The identification token is missing",
                FR: "Le token d'identification est manquant",
            },
            CODE_HTTP: 401,
        },
        EXPIRED_TOKEN: {
            MESSAGE: {
                EN: "The identification token is expired",
                FR: "Le token d'identification a expiré",
            },
            CODE_HTTP: 401,
        },
        INVALID_CREDDENTIALS: {
            MESSAGE: {
                EN: "The credentials are invalids",
                FR: "Les indentifiants fournis sont invalides",
            },
            CODE_HTTP: 401,
        },
        NOT_ADMIN: {
            MESSAGE: {
                EN: "Need administrator privileges",
                FR: "Nécéssite des droits administrateur",
            },
            CODE_HTTP: 403,
        },
        ACCOUNT_DESACTIVATED: {
            MESSAGE: {
                EN: "The account is desactivated",
                FR: "Le compte utilisateur est désactivé",
            },
            CODE_HTTP: 401,
        },
        FORBIDDEN: {
            MESSAGE: {
                EN: "Unable to edit another profile",
                FR: "Impossible d'éditer un autre profil",
            },
            CODE_HTTP: 403,
        },
        MISSING_ID: {
            MESSAGE: {
                EN: "The user id is missing",
                FR: "L'id utilisateur est manquant",
            },
            CODE_HTTP: 400,
        },
        INVALID_ID: {
            MESSAGE: {
                EN: "The user id is invalid",
                FR: "L'id utilisateur est invalide",
            },
            CODE_HTTP: 422,
        },
        MISSING_PROFILE: {
            MESSAGE: {
                EN: "The user profile is missing",
                FR: "Le profil utilisateur est manquant",
            },
            CODE_HTTP: 400,
        },
    },
    REGISTER: {
        ACTION: "Create a new user",
        SUCCESS: {
            MESSAGE: {
                EN: "User created with success",
                FR: "Utilisateur créé avec succès",
            },
            CODE_HTTP: 201,
        },
        ERROR: {
            MESSAGE: {
                EN: "Failed to create a new user",
                FR: "Impossible de créer un nouvel utilisateur",
            },
            CODE_HTTP: 500,
        },
    },
    LOGIN: {
        ACTION: "Log in an user",
        SUCCESS: {
            MESSAGE: {
                EN: "user logged with success",
                FR: "utilisateur identifié avec succès",
            },
            CODE_HTTP: 200,
        },
        ERROR: {
            MESSAGE: {
                EN: "failed to log the user",
                FR: "Impossible d'identifier l'utilisateur",
            },
            CODE_HTTP: 500,
        },
    },
    FETCH_PROFILE: {
        ACTION: "Retrieve its profile",
        SUCCESS: {
            MESSAGE: {
                EN: "User profile retrieved with success",
                FR: "Profil utilisateur récupéré avec succès",
            },
            CODE_HTTP: 200,
        },
        ERROR: {
            MESSAGE: {
                EN: "Failed to retreved the user profile",
                FR: "Impossible de récupérer le profil utilisateur",
            },
            CODE_HTTP: 500,
        },
    },
    UPDATE_PROFILE: {
        ACTION: "Update user profile",
        SUCCESS: {
            MESSAGE: {
                EN: "User profile updated with success",
                FR: "Profil utilisateur mis à jour avec succès",
            },
            CODE_HTTP: 200,
        },
        ERROR: {
            MESSAGE: {
                EN: "Failed to update the user profile",
                FR: "Impossible de mettre à jour le profil utilisateur",
            },
            CODE_HTTP: 500,
        },
        NOTICES: {
            INVALID_FIELD: {
                MESSAGE: {
                    EN: "The given field of the user's profile does not exist and has been ignored",
                    FR: "Le champs du profil utilisateur fourni n'existe pas et a été ignoré"
                },
            },
        },
    },
    DELETE: {
        ACTION: "Delete user",
        SUCCESS: {
            MESSAGE: {
                EN: "User deleted with success",
                FR: "utilisateur supprimé avec succès",
            },
            CODE_HTTP: 204,
        },
        ERROR: {
            MESSAGE: {
                EN: "Failed to delete the user",
                FR: "Echec lors de la suppression de l'utilisateur",
            },
            CODE_HTTP: 500,
        },
    },
};
