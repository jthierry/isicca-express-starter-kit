const config = require('config');

module.exports = {
  "development": {
    "username": config.db.USER,
    "password": config.db.PASSWORD,
    "database": config.db.NAME,
    "host": config.db.URL,
    "dialect": "mysql",
    "logging": true
  },
  "testing": {
    "username": config.db.USER,
    "password": config.db.PASSWORD,
    "database": config.db.NAME,
    "host": config.db.URL,
    "dialect": "mysql",
    "logging": false
  },
  "production": {
    "username": config.db.USER,
    "password": config.db.PASSWORD,
    "database": config.db.NAME,
    "host": config.db.URL,
    "dialect": "mysql",
    "logging": false
  }
}
