/**
 * @swagger
 * securityDefinitions:
 *  Bearer:
 *    name: Authorization
 *    type: apiKey
 *    in: header

 * tags:
 *   - name: Auth
 *     description: Authentification for users
 *   - name: Account
 *     description: User account management
 *   - name: Admin
 *     description: Administration toolkit (only for administrators)
 *   - name: Logs
 *     description: Logs history

 * responses:
 *  200_userprofile:
 *    description: Success - Getting user's profile
 *    schema:
 *      type: object
 *      properties:
 *        message:
 *          type: string
 *        metadata:
 *          $ref: '#/definitions/Metadata'
 *        results:
 *          type: array
 *          items:
 *              $ref: '#/definitions/User'
 *        error:
 *          type: object
 *        notices:
 *          type: array
 *          items:
 *            type: string
 *  200_usersession:
 *    description: Success - Getting user's session & profile
 *    schema:
 *      type: object
 *      properties:
 *        message:
 *          type: string
 *        metadata:
 *          $ref: '#/definitions/Metadata'
 *        results:
 *          type: array
 *          items:
 *              $ref: '#/definitions/User_session'
 *        error:
 *          type: object
 *        notices:
 *          type: array
 *          items:
 *            type: string
 *  200_users:
 *    description: Success - Getting array of users
 *    schema:
 *      type: object
 *      properties:
 *        message:
 *          type: string
 *        metadata:
 *          $ref: '#/definitions/Metadata'
 *        results:
 *          type: array
 *          items:
 *            $ref: '#/definitions/User'
 *        error:
 *          type: object
 *        notices:
 *          type: array
 *          items:
 *            type: string
 *  200_logs:
 *    description: Success - Getting array of logs
 *    schema:
 *      type: object
 *      properties:
 *        message:
 *          type: string
 *        metadata:
 *          $ref: '#/definitions/Metadata'
 *        results:
 *          type: array
 *          items:
 *            $ref: '#/definitions/Log'
 *        error:
 *          type: object
 *        notices:
 *          type: array
 *          items:
 *            type: string
 *  201:
 *    description: 'Created with sucess' 
 *  204:
 *    description: 'Success with no content'
 *  400:
 *    description: 'Bad request : syntax error in parameter'
 *    schema:
 *      $ref: '#/definitions/Error'
 *  401:
 *    description: 'Unauthorized access'
 *    schema:
 *      $ref: '#/definitions/Error'
 *  403:
 *    description: 'Forbidden'
 *    schema:
 *      $ref: '#/definitions/Error'
 *  404:
 *    description: 'Not found'
 *    schema:
 *      $ref: '#/definitions/Error'
 *  409:
 *    description: 'Conflict with existing resource'
 *    schema:
 *      $ref: '#/definitions/Error'
 *  422:
 *    description: 'Bad request : semantic error in parameter'
 *    schema:
 *      $ref: '#/definitions/Error'
 *  500:
 *    description: 'Internal unknow error'
 *    schema:
 *      $ref: '#/definitions/Error'

 * parameters:
 *  logId:
 *    name: Log_id
 *    description: Id de log
 *    in: path
 *    required: true
 *    type: string
 *  limit:
 *    name: limit
 *    description: Number of maximum results
 *    in: query
 *    type: integer
 *  sort:
 *    name: sort
 *    description: "Sort results by fields. Can have one or many fields separated by commas.\r\n <b>Example: sort=name(asc),date(desc)</b>"
 *    in: query
 *    type: string
 *    enum:
 *      - asc
 *      - desc
 *  select:
 *    name: select
 *    description: "Include or exclude fields to display. Can have one or many fields separated by commas.\r\n <b>Example 1: select=include(name,date)</b>\r\n<b>Example 2: fields=select(date)</b>"
 *    in: query
 *    type: string
 *  filter:
 *    name: filter
 *    description: "Filter results by fields. Can have one or more filters, separated by commas.\r\n<b>Example 1: filter=date(>2018-10-01),code(200|201|202),value(<500|>200)</b>\r\n<b>Example 2: filter=date(2018-10-01|2018-10-02),name=(jacky|thierry)</b>"
 *    in: query
 *    type: string
 *  range:
 *    name: range
 *    description: "Select the range of results to display.\r\n<b>Exemple: range=0,10</n>"
 *    in: query
 *    type: string
 *  id:
 *    name: User_id
 *    description: User's unique Id
 *    in: path
 *    required: true
 *    type: string
 *  user_id:
 *    name: ID
 *    description: User's unique Id
 *    in: formData
 *    required: true
 *    type: string
 *  user_email:
 *    name: Email
 *    description: User's email
 *    in: formData
 *    required: true
 *    type: string
 *    format: email
 *    pattern: '/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'
 *  user_password:
 *    name: Password
 *    description: User's password
 *    in: formData
 *    required: true
 *    type: string
 *    format: password
 *    pattern: '/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/'
 *  user_firstname:
 *    name: First name
 *    description: User's first name
 *    in: formData
 *    required: false
 *    type: string
 *  user_lastname:
 *    name: Last name
 *    description: User's Last name
 *    in: formData
 *    required: false
 *    type: string
 *  user_role:
 *    name: Role
 *    description: User's role
 *    in: formData
 *    required: false
 *    type: string
 *    enum: [user, admin]
 *  user_active:
 *    name: Status
 *    description: User's activation
 *    in: formData
 *    required: false
 *    type: boolean
 
*/
