const config = require('config');

module.exports = {
  swaggerDefinition: {
    info: {
      version: config.app.API_VERSION,
      title: "ISICCA Node's Starter Kit",
      description: 'Template for node projets',
      termsOfService: 'None',
      contact: {
        name: 'Jacky Thierry',
        url: 'https://www.isicca.com',
        email: 'jacky@isicca.fr'
      },
      license: {
        name: 'Licence GPLv3',
        url: 'http://www.gnu.org/licenses/gpl.html'
      }
    },
    host: 'localhost:3000',
    basePath: '/api/' + config.app.API_VERSION + '/',
    schemes: ['http', 'https'],
    consumes: ['application/x-www-form-urlencoded'],
    produces: ['application/json']
  },
  apis: ['./config/swagger/definitions.js', './routes/*.js', './models/*.js']
};
