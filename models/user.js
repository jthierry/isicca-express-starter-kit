/**
 * @swagger
 * definition:
 *  User:
 *    type: object
 *    properties:
 *      userId:
 *        $ref: '#/parameters/user_id'
 *      email:
 *        $ref: '#/parameters/user_email'
 *      firstName:
 *        $ref: '#/parameters/user_firstname'
 *      lastName:
 *        $ref: '#/parameters/user_lastname'
 *      role:
 *        $ref: '#/parameters/user_role'
 *      active:
 *        $ref: '#/parameters/user_active'
 *  User_profile:
 *    required: [firstName, lastName]
 *    description: User's profile
 *    properties:
 *      firstName:
 *        $ref: '#/parameters/user_firstname'
 *      lastName:
 *        $ref: '#/parameters/user_lastname'
 *  User_session:
 *    description: User's session infos
 *    properties:
 *      token:
 *        type: string
 *      expiresIn:
 *        type: integer
 *      profile:
 *        $ref: '#/definitions/User'
*/


module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4, primaryKey: true },
    email: { type: DataTypes.STRING(191), allowNull: false, unique: true },
    password: { type: DataTypes.STRING },
    firstName: { type: DataTypes.STRING },
    lastName: { type: DataTypes.STRING, allowNull: false },
    role: { type: DataTypes.STRING, allowNull: false, isIn: [['admin', 'user']], defaultValue: 'user' },
    active: { type: DataTypes.BOOLEAN, allowNull: true, defaultValue: true }
}, {freezeTableName: true});
  User.associate = function(models) {
  };
  return User;
};