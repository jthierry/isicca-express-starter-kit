/**
 * @swagger
 * definition:
 *  Metadata:
 *    properties:
 *      url:
 *        type: string
 *      query:
 *        type: object
 *        properties:
 *          filter:
 *            type: object
 *          select:
 *            type: object
 *          sort:
 *            type: object
 *          limit:
 *            type: integer
 *          range:
 *            type: object
 *      links:
 *        type: object
 *      error:
 *        type: integer
 *      notices:
 *        type: integer
 *      results:
 *        type: integer
 *  Error:
 *    properties:
 *      message:
 *        type: string
 *      datas:
 *        type: object
 *      error:
 *        type: object
 *        properties:
 *          message:
 *            type: string
 *  LogError:
 *    properties:
 *      name:
 *        type: string
 *      message:
 *        type: string
 *      internal:
 *        type: boolean
 *      stack:
 *        type: string
 *  Log:
 *    properties:
 *      date:
 *        type: string
 *        format: date-time
 *      req:
 *        type: object
 *        properties:
 *          params:
 *            type: object
 *          query:
 *            type: object
 *          body:
 *            type: object
 *          headers:
 *            type: object
 *      res:
 *        type: object
 *        properties:
 *          headers:
 *            type: object
 *          httpVersion:
 *            type: string
 *      user:
 *        $ref: '#/definitions/User'
 *      action:
 *        type: string
 *      success:
 *        type: boolean
 *      target:
 *        type: string
 *      httpCode:
 *        type: number
 *      reqMethod:  
 *        type: string
 *      reqUrl:
 *        type: string
 *      reqUserAgent:
 *        type: string
 *      reqIp:
 *        type: string
 *      resHttpCode:
 *        type: number
 *      resTime:
 *        type: number
 *      error:
 *        $ref: '#/definitions/LogError'
 *      notices:
 *        type: array
 *        items:
 *          type: string
*/

module.exports = (sequelize, DataTypes) => {
  const Log = sequelize.define('Log', {
    id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4, primaryKey: true },
    date: { type: DataTypes.DATE(6), allowNull: false, defaultValue: DataTypes.NOW },
    req: { type: DataTypes.TEXT, allowNull: true },
    res: { type: DataTypes.TEXT, allowNull: true },
    action: { type: DataTypes.STRING, allowNull: true },
    success: { type: DataTypes.BOOLEAN, allowNull: true },
    resHttpCode: { type: DataTypes.INTEGER, allowNull: true },
    reqMethod: { type: DataTypes.STRING, allowNull: true },
    reqUrl: { type: DataTypes.STRING, allowNull: true },
    reqUserAgent: { type: DataTypes.STRING, allowNull: true },
    reqIp: { type: DataTypes.STRING, allowNull: true },
    resTime: { type: DataTypes.FLOAT, allowNull: true },
    error: { type: DataTypes.TEXT, allowNull: true },
    notices: { type: DataTypes.TEXT, allowNull: true },
  }, {freezeTableName: true});
  Log.associate = function (models) {
    Log.belongsTo(models.User, {foreignKey: 'user', targetKey: 'id'});
  };
  return Log;
};