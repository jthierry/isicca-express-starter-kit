# Isicca starter Kit for Node/Express JS applications

This project is a starter kit to start new API express projects.
It includes :

* Users management & authentification
* Logs saving in console, file, and/or database
* Administration for users and logs
* Complete REST API with full query strings, hateoas, cors
* ODM (mongoose) and ORM (sequelize) for requesting databases
* Security with SSL & HTTP headers
* Environments configuration
* Internationalization of messages & responses (english and french at the moment)
* Unit tests & code coverage
* Documentation OpenAPI 2.0 - Swagger
* ES6 syntax with await / aync functions
* KISS & DRY practices

## Getting started

### Prerequisites

The following packages must be globally installed

* Node in version 10 or above
* Nodemon

```bash
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt install nodejs build-essential
sudo npm install -g nodemon
```

### Install

Clone the repository

```bash
git clone git@git.isicca.fr:templates/express.git
```

### Install & update the dependencies 

```javascript
npm install
npm update
```

### Configure

Copy the config template file into development.json et testing.json files

```bash
cp config/template config/development.json
cp config/template config/testing.json
```

Fill or change the values according to your needs : 

* app : application's variables
 * PORT : port's numer to use for running the api
 * SSL : activate SSL
 * API_VERSION : verion's of the api 
 * LANG : Langage of the messages and responses (EN or FR)
 * METADATA : display metadata in response

* ssl : certificate paths
 * CERT_PATH : path to ssl certificate
 * KEY_PATH : path to SSL key

* db : database's variables
 * TYPE : type of db server to use (MONGO or MYSQL)
 * NAME : name of the database to use/create
 * USER : user name to use to connect to the database
 * PASSWORD : user password to use to connect to the database
 * URL : url of the database's server
 * PORT : port of the database's server
 * LOG : display SQL request in console (only for MYSQL)

* log : logging's variables
 * CONSOLE : display requests logs in console
 * DB : insert all request and results logs in database
 * FILE : write request and results summaries in files
 * FILE_FORMAT : morgan's format for writing logs in files

* user : users identification and authorisation's variables
 * ALLOW_CREATION_SELFADMIN : allow users to register an admin account (if false, only an admin can give admin rights)
 * MIN_SIZE_PASSWORD : minimal lenght of user's password
 * JWT_KEY : a random long secret passphrase with miniscules, majuscules, numbers and special caracters
 * JWT_EXPIRATIONTIME_HOUR : delay of token's validation (in hour)

* query : default values for query strings
 * SORT_DEFAULT : fields and way to sort results
 * RANGE_MAX : max results allowed for a request

### Create databases and access :

#### MongoDB :

```sql
use {DBDEV}
db.createUser({user: '{USERDEV}', pwd: '{PASSWORDDEV}', roles: ['readWrite']})
use {DBTEST}
db.createUser({user: '{USERTEST}', pwd: '{PASSWORDTEST}', roles: ['readWrite']})
```

#### MYSQL :
```sql
CREATE USER '{USERDEV}'@'localhost' IDENTIFIED BY '{PASSWORDDEV}';
CREATE DATABASE {DBDEV};
GRANT ALL PRIVILEGES ON {DBDEV}.* TO '{USERDEV}'@'localhost';
CREATE USER '{USERTEST}'@'localhost' IDENTIFIED BY '{PASSWORDTEST}';
CREATE DATABASE {DBTEST};
GRANT ALL PRIVILEGES ON {DBTEST}.* TO '{USERTEST}'@'localhost';
FLUSH PRIVILEGES;
```

## Test the app

This starter kit already has more than 150 unit tests ready. You can launch them, and check the code coverage report with the following command :

```javacript
npm test
```

## Swagger

This starter kit is fully compatible with OpenAPI 2.0 (aka Swagger).
The swagger file can be accessed at /swagger.json when the app is running in development environment.

## Response format

Each response is associated with an http code and centralized in the file `helpers/messages.js`
The reponses are internationalized (for now in english and french).

All responses (success & errors) will have the same architecture :

* Message (string) : Description of the response
* Metadata : informations about request and response (only if activated in config)
* Results (array) : Results for the request (null if error)
* Error (object) : Error informations (null if success)
 * Message (string) : Description of the error

See the swagger for more informations

## HTTP codes

11 http codes are used in this started kit.
They all are gathered with the corresponding responses in `helpers/messages.js`
Each response has its own http code.

List of used http codes :

* 200 : Success with results
* 201 : Created with success
* 204 : Success with no content
* 206 : Success with partial results
* 400 : Bad request - syntax error in parameter
* 401 : Unauthorized access
* 403 : Forbidden
* 404 : Endpoint not found
* 409 : Conflict with existing resource
* 422 : Bad request - semantic error in parameter
* 500 : Internal unknow error

## Licencing

The source code is licensed under GPL v3. License is available [here](/COPYING)

## Author

This starter kit has been developped by Jacky Thierry :

* [Linkedin](https://www.linkedin.com/in/jackythierry)
* [Blog](https://www.isicca.com)

## Todo

### Evolutions
* Refactor range query system for SGDBR
* Rewrite more readable query strings in response metadata
* Add HTTP2 support
* Add XML output

### New features
* Add ACL and roles system
* Add OAuth2 authentification & user gestion
* Generate models, migrations & seeds from json inputs
* Autogenerate swagger

### Devops
* Industrialize unit tests
* Add Travis for CI
* Git architecture
* UML workflows
* Add  docker containers