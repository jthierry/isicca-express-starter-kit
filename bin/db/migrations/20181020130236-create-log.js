'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Log', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      date: {
        allowNull: false,
        defaultValue: Sequelize.NOW,
        type: Sequelize.DATE
      },
      req: {
        type: Sequelize.TEXT
      },
      res: {
        type: Sequelize.TEXT
      },
      action: {
        type: Sequelize.STRING
      },
      success: {
        type: Sequelize.BOOLEAN
      },
      resHttpCode: {
        type: Sequelize.INTEGER
      },
      reqMethod: {
        type: Sequelize.STRING
      },
      ReqUrl: {
        type: Sequelize.STRING
      },
      reqUserAgent: {
        type: Sequelize.STRING
      },
      reqIp: {
        type: Sequelize.STRING
      },
      resTime: {
        type: Sequelize.FLOAT
      },
      error: {
        type: Sequelize.TEXT
      },
      notices: {
        type: Sequelize.TEXT
      },
      user: {
        type: Sequelize.UUID,
        allowNull: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE(6)
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE(6)
      }
    })
      // .then(() => queryInterface.addConstraint('Log', ['user'], {
      //   type: 'FOREIGN KEY',
      //   name: 'user_log_FK_user',
      //   references: {
      //     table: 'User',
      //     field: 'id',
      //   },
      //   onDelete: 'SET NULL',
      //   onUpdate: 'SET NULL',
      // }))
      ;
  },
  down: (queryInterface, Sequelize) => {
    // return queryInterface.sequelize.query('ALTER TABLE `Log` DROP FOREIGN KEY IF EXISTS `user_log_FK_user`;').then(() => queryInterface.dropTable('Log'));
    return queryInterface.dropTable('Log');
  }
};