const express = require('express');
const router = express.Router();

const authCtl = require('../controllers/auth');
const routeFieldsMdw = require('../middlewares/route-fields');
const routeResponseMdw = require('../middlewares/route-response');

const MESS = require('../config/messages');

/**
 * @swagger
 * /auth/register:
 *  post:
 *    summary: 'Create a new user'
 *    tags: [Auth]
 *    parameters:
 *      - $ref: '#/parameters/user_email'
 *      - $ref: '#/parameters/user_password'
 *      - $ref: '#/parameters/user_firstname'
 *      - $ref: '#/parameters/user_lastname'
 *    responses:
 *      204:
 *        $ref: '#/responses/204'
 *      400:
 *        $ref: '#/responses/400'
 *      409:
 *        $ref: '#/responses/409'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
router.post(
  '/register',
  [routeFieldsMdw.isEmail, routeFieldsMdw.isPassword, routeFieldsMdw.isUserCreationFields],
  [routeResponseMdw.populate(MESS.USER.REGISTER)],
  authCtl.createUser
);

/**
 * @swagger
 * /auth/login:
 *  post:
 *    summary: Log in an user
 *    tags: [Auth]
 *    parameters:
 *      - $ref: '#/parameters/user_email'
 *      - $ref: '#/parameters/user_password'
 *    responses:
 *      200:
 *        $ref: '#/responses/200_usersession'
 *      400:
 *        $ref: '#/responses/400'
 *      401:
 *        $ref: '#/responses/401'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
router.post(
  '/login',
  [routeFieldsMdw.isEmail, routeFieldsMdw.isPassword],
  [routeResponseMdw.populate(MESS.USER.LOGIN)],
  authCtl.loginUser
);

module.exports = router;