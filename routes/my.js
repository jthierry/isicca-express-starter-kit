const express = require('express');
const router = express.Router();

const userAuthMdw = require('../middlewares/user-auth');
const userProfileRtr = require('./user-profile');
const logRtr = require('./log');

router.use('/account', [userAuthMdw.isAuth], userProfileRtr);
router.use('/logs', [userAuthMdw.isAuth], [userAuthMdw.logUser], logRtr);

module.exports = router;
