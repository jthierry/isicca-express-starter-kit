const express = require('express');
const router = express.Router();

const authCtl = require('../controllers/auth');
const adminCtl = require('../controllers/admin');
const userAuthMdw = require('../middlewares/user-auth');
const userProfileMdw = require('../middlewares/user-profile');
const routeFieldsMdw = require('../middlewares/route-fields');
const routeResponseMdw = require('../middlewares/route-response');
const routeQuerystringMdw = require('../middlewares/route-querystring');

const MESS = require('../config/messages');

/**
 * @swagger
 * /users:
 *  get:
 *    summary: List all users
 *    tags: [Admin]
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - $ref: '#/parameters/limit'
 *      - $ref: '#/parameters/sort'
 *      - $ref: '#/parameters/select'
 *      - $ref: '#/parameters/filter'
 *      - $ref: '#/parameters/range'
 *    responses:
 *      200:
 *        $ref: '#/responses/200_users'
 *      401:
 *        $ref: '#/responses/401'
 *      403:
 *        $ref: '#/responses/403'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
router.get(
  '/',
  [userAuthMdw.isAuth, userAuthMdw.isAdmin],
  [routeQuerystringMdw.process('User'), routeResponseMdw.populate(MESS.ADMIN.FETCH_ALL)],
  adminCtl.fetchUsers
);

/**
 * @swagger
 * /users:
 *  post:
 *    summary: Add an user
 *    tags: [Admin]
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - $ref: '#/parameters/user_email'
 *      - $ref: '#/parameters/user_password'
 *      - $ref: '#/parameters/user_firstname'
 *      - $ref: '#/parameters/user_lastname'
 *    responses:
 *      204:
 *        $ref: '#/responses/204'
 *      400:
 *        $ref: '#/responses/400'
 *      401:
 *        $ref: '#/responses/401'
 *      403:
 *        $ref: '#/responses/403'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
router.post(
  '/',
  [
    userAuthMdw.isAuth,
    userAuthMdw.isAdmin,
    routeFieldsMdw.isEmail,
    routeFieldsMdw.isPassword,
    routeFieldsMdw.isUserCreationFields
  ],
  [routeResponseMdw.populate(MESS.USER.REGISTER)],
  authCtl.createUser
);

/**
 * @swagger
 * /users/{User_id}/add-admin-role:
 *  patch:
 *    summary: Add admin role to an user
 *    tags: [Admin]
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - $ref: '#/parameters/id'
 *    responses:
 *      204:
 *        $ref: '#/responses/204'
 *      400:
 *        $ref: '#/responses/400'
 *      401:
 *        $ref: '#/responses/401'
 *      403:
 *        $ref: '#/responses/403'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
router.patch(
  '/:id/add-admin-role',
  [userAuthMdw.isAuth, userAuthMdw.isAdmin, routeFieldsMdw.isDbId],
  [userProfileMdw.updateRole('admin')],
  [routeResponseMdw.populate(MESS.USER.UPDATE_PROFILE)],
  adminCtl.changeRoleUser
);

/**
* @swagger
* /users/{User_id}/remove-admin-role:
*  patch:
*    summary: Remove admin role to an user
*    tags: [Admin]
*    security:
*      - Bearer: []
*    parameters:
*      - $ref: '#/parameters/id'
*    responses:
*      204:
*        $ref: '#/responses/204'
*      400:
*        $ref: '#/responses/400'
*      401:
*        $ref: '#/responses/401'
*      403:
*        $ref: '#/responses/403'
*      422:
*        $ref: '#/responses/422'
*      500:
*        $ref: '#/responses/500'
*/
router.patch(
  '/:id/remove-admin-role',
  [userAuthMdw.isAuth, userAuthMdw.isAdmin, routeFieldsMdw.isDbId],
  [userProfileMdw.updateRole('user')],
  [routeResponseMdw.populate(MESS.USER.UPDATE_PROFILE)],
  adminCtl.changeRoleUser
);

const UserProfileRouter = require('./user-profile');
router.use('/:id', [userAuthMdw.isAuth, userAuthMdw.isAdmin, routeFieldsMdw.isDbId], UserProfileRouter);

module.exports = router;
