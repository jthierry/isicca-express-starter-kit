const express = require('express');
const router = express.Router();

const userCtl = require('../controllers/user');
const userProfileMdw = require('../middlewares/user-profile');
const routeFieldsMdw = require('../middlewares/route-fields');
const routeResponseMdw = require('../middlewares/route-response');

const MESS = require('../config/messages');

/**
 * @swagger
 * /users/{User_id}:
 *  get:
 *    summary: fetch one user
 *    tags: [Admin]
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - $ref: '#/parameters/id'
 *    responses:
 *      200:
 *        $ref: '#/responses/200_users'
 *      401:
 *        $ref: '#/responses/401'
 *      403:
 *        $ref: '#/responses/403'
 *      404:
 *        $ref: '#/responses/404'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
/**
 * @swagger
 * /my/account:
 *  get:
 *    summary: fetch profile
 *    tags: [Account]
 *    security:
 *      - Bearer: []
 *    responses:
 *      200:
 *        $ref: '#/responses/200_users'
 *      401:
 *        $ref: '#/responses/401'
 *      403:
 *        $ref: '#/responses/403'
 *      404:
 *        $ref: '#/responses/404'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
router.get(
    '/',
    [routeResponseMdw.populate(MESS.USER.FETCH_PROFILE)],
    userCtl.getProfile
);

/**
 * @swagger
 * /users/{User_id}:
 *  put:
 *    summary: Update the user's profile
 *    tags: [Admin]
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - $ref: '#/parameters/id'
 *      - name: Profile
 *        in: body
 *        required: true
 *        schema:
 *          $ref: '#/definitions/User_profile'
 *    responses:
 *      204:
 *        $ref: '#/responses/204'
 *      400:
 *        $ref: '#/responses/400'
 *      401:
 *        $ref: '#/responses/401'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
/**
 * @swagger
 * /my/account:
 *  put:
 *    summary: Update the user's profile
 *    tags: [Account]
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - name: Profile
 *        in: body
 *        required: true
 *        schema:
 *          $ref: '#/definitions/User_profile'
 *    responses:
 *      204:
 *        $ref: '#/responses/204'
 *      400:
 *        $ref: '#/responses/400'
 *      401:
 *        $ref: '#/responses/401'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
router.put(
    '/',
    [userProfileMdw.updateProfile],
    [routeResponseMdw.populate(MESS.USER.UPDATE_PROFILE)],
    userCtl.updateProfile
);

/**
 * @swagger
 * /users/{User_id}:
 *  delete:
 *    summary: Delete an user
 *    tags: [Admin]
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - $ref: '#/parameters/id'
 *    responses:
 *      204:
 *        $ref: '#/responses/204'
 *      400:
 *        $ref: '#/responses/400'
 *      401:
 *        $ref: '#/responses/401'
 *      403:
 *        $ref: '#/responses/403'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
/**
 * @swagger
 * /my/account:
 *  delete:
 *    summary: Delete the user account
 *    tags: [Account]
 *    security:
 *      - Bearer: []
 *    responses:
 *      204:
 *        $ref: '#/responses/204'
 *      400:
 *        $ref: '#/responses/400'
 *      401:
 *        $ref: '#/responses/401'
 *      403:
 *        $ref: '#/responses/403'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
router.delete(
    '/',
    [routeResponseMdw.populate(MESS.USER.DELETE)],
    userCtl.deleteUser
);

/**
 * @swagger
 * /users/{User_id}/active:
 *  patch:
 *    summary: Active an user
 *    tags: [Admin]
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - $ref: '#/parameters/id'
 *    responses:
 *      204:
 *        $ref: '#/responses/204'
 *      400:
 *        $ref: '#/responses/400'
 *      401:
 *        $ref: '#/responses/401'
 *      403:
 *        $ref: '#/responses/403'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
/**
 * @swagger
 * /my/account/active:
 *  patch:
 *    summary: Active the user
 *    tags: [Account]
 *    security:
 *      - Bearer: []
 *    responses:
 *      204:
 *        $ref: '#/responses/204'
 *      400:
 *        $ref: '#/responses/400'
 *      401:
 *        $ref: '#/responses/401'
 *      403:
 *        $ref: '#/responses/403'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
router.patch(
    '/active',
    [userProfileMdw.updateActiveState(true)],
    [routeResponseMdw.populate(MESS.USER.UPDATE_PROFILE)],
    userCtl.changeActiveState
);

/**
 * @swagger
 * /users/{User_id}/unactive:
 *  patch:
 *    summary: Desactive an user
 *    tags: [Admin]
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - $ref: '#/parameters/id'
 *    responses:
 *      204:
 *        $ref: '#/responses/204'
 *      400:
 *        $ref: '#/responses/400'
 *      401:
 *        $ref: '#/responses/401'
 *      403:
 *        $ref: '#/responses/403'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
/**
 * @swagger
 * /my/account/unactive:
 *  patch:
 *    summary: Desactive the user
 *    tags: [Account]
 *    security:
 *      - Bearer: []
 *    responses:
 *      204:
 *        $ref: '#/responses/204'
 *      400:
 *        $ref: '#/responses/400'
 *      401:
 *        $ref: '#/responses/401'
 *      403:
 *        $ref: '#/responses/403'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
router.patch(
    '/unactive',
    [userProfileMdw.updateActiveState(false)],
    [routeResponseMdw.populate(MESS.USER.UPDATE_PROFILE)],
    userCtl.changeActiveState
);


/**
 * @swagger
 * /users/{User_id}/change-password:
 *  patch:
 *    summary: Update the user's password
 *    tags: [Admin]
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - $ref: '#/parameters/id'
 *      - $ref: '#/parameters/user_password'
 *    responses:
 *      204:
 *        $ref: '#/responses/204'
 *      400:
 *        $ref: '#/responses/400'
 *      401:
 *        $ref: '#/responses/401'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
/**
 * @swagger
 * /my/account/change-password:
 *  patch:
 *    summary: Update the user's password
 *    tags: [Account]
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - $ref: '#/parameters/user_password'
 *    responses:
 *      204:
 *        $ref: '#/responses/204'
 *      400:
 *        $ref: '#/responses/400'
 *      401:
 *        $ref: '#/responses/401'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
router.patch(
    '/change-password',
    [routeFieldsMdw.isPassword],
    [routeResponseMdw.populate(MESS.USER.UPDATE_PROFILE)],
    userCtl.updatePassword
);

module.exports = router;
