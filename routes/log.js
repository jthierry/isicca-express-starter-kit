const express = require('express');
const router = express.Router();

const logCtl = require('../controllers/log');
const routeFieldsMdw = require('../middlewares/route-fields');
const routeResponseMdw = require('../middlewares/route-response');
const routeQuerystringMdw = require('../middlewares/route-querystring');

const MESS = require('../config/messages');

/**
 * @swagger
 * /logs/:
 *  get:
 *    summary: Fetch all logs
 *    tags: [Admin]
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - $ref: '#/parameters/limit'
 *      - $ref: '#/parameters/sort'
 *      - $ref: '#/parameters/select'
 *      - $ref: '#/parameters/filter'
 *      - $ref: '#/parameters/range'
 *    responses:
 *      200:
 *        $ref: '#/responses/200_logs'
 *      400:
 *        $ref: '#/responses/400'
 *      409:
 *        $ref: '#/responses/409'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
/**
 * @swagger
 * /my/logs/:
 *  get:
 *    summary: Fetch all user logs
 *    tags: [Logs]
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - $ref: '#/parameters/limit'
 *      - $ref: '#/parameters/sort'
 *      - $ref: '#/parameters/select'
 *      - $ref: '#/parameters/filter'
 *      - $ref: '#/parameters/range'
 *    responses:
 *      200:
 *        $ref: '#/responses/200_logs'
 *      400:
 *        $ref: '#/responses/400'
 *      409:
 *        $ref: '#/responses/409'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
router.get(
  '/',
  [routeQuerystringMdw.process('Log'), routeResponseMdw.populate(MESS.LOG.FETCH_ALL)],
  logCtl.fetchAll
);


/**
 * @swagger
 * /logs/{Log_id}:
 *  get:
 *    summary: Fetch a given log
 *    tags: [Admin]
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - $ref: '#/parameters/logId'
 *    responses:
 *      200:
 *        $ref: '#/responses/200_logs'
 *      401:
 *        $ref: '#/responses/401'
 *      403:
 *        $ref: '#/responses/403'
 *      404:
 *        $ref: '#/responses/404'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
/**
 * @swagger
 * /my/logs/{Log_id}:
 *  get:
 *    summary: Fetch a given user's log
 *    tags: [Logs]
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - $ref: '#/parameters/logId'
 *    responses:
 *      200:
 *        $ref: '#/responses/200_logs'
 *      401:
 *        $ref: '#/responses/401'
 *      403:
 *        $ref: '#/responses/403'
 *      404:
 *        $ref: '#/responses/404'
 *      422:
 *        $ref: '#/responses/422'
 *      500:
 *        $ref: '#/responses/500'
*/
router.get(
  '/:id',
  [routeFieldsMdw.isDbId],
  [routeResponseMdw.populate(MESS.LOG.FETCH_ONE)],
  logCtl.fetchOne
);

module.exports = router;