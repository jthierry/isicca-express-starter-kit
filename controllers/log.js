const Log = require('../models/').Log;
const MESS = require('../config/messages');
const responseHlp = require('../helpers/response');

exports.fetchAll = async (req, res, next) => {
    try {
        const logs = await Log.findAll(req.locals.customquery);
        responseHlp.success(req, res, next, logs);
    } catch (err) {
        /* istanbul ignore next */
        responseHlp.error(req, res, next, err);
    }
}

exports.fetchOne = async (req, res, next) => {
    try {
        const result = await Log.findById(req.params.id);
        if (!result) {
            throw MESS.LOG.ERRORS.INVALID_ID;
        }
        if (req.locals.user.role !== 'admin' && result.user.toString() !== req.locals.user.id) {
            throw MESS.USER.ERRORS.NOT_ADMIN;
        }
        responseHlp.success(req, res, next, result);
    } catch (err) {
        /* istanbul ignore next */
        responseHlp.error(req, res, next, err);
    }
}
