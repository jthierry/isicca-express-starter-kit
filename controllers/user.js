const bcrypt = require('bcrypt');

const User = require('../models/').User;
const sqlHlp = require('../helpers/sql');
const responseHlp = require('../helpers/response');
const MESS = require('../config/messages');

exports.getProfile = async (req, res, next) => {
  try {
    const profile = await User.findById(req.locals.id, { attributes: sqlHlp.updateSqlAttributes(req.locals, 'User') });
    if (!profile) {
      throw MESS.USER.ERRORS.INVALID_ID;
    }
    responseHlp.success(req, res, next, profile);
  } catch (err) {
    /* istanbul ignore next */
    responseHlp.error(req, res, next, err);
  }
};

exports.changeActiveState = async (req, res, next) => {
  try {
    const result = await User.update(req.locals.profile.activeState, { where: { id: req.locals.id } });
    if (result[0] !== 1) {
      throw MESS.USER.ERRORS.INVALID_ID;
    }
    responseHlp.success(req, res, next);
  } catch (err) {
    /* istanbul ignore next */
    responseHlp.error(req, res, next, err);
  }
};

exports.updateProfile = async (req, res, next) => {
  try {
    const result = await User.update(req.locals.profile, { where: { id: req.locals.id } });
    if (result[0] !== 1) {
      throw MESS.USER.ERRORS.INVALID_ID;
    }
    const profile = await User.findById(req.locals.id, { attributes: sqlHlp.updateSqlAttributes(req.locals, 'User') });
    responseHlp.success(req, res, next, profile);
  } catch (err) {
    /* istanbul ignore next */
    responseHlp.error(req, res, next, err);
  }
};

exports.updatePassword = async (req, res, next) => {
  try {
    const password = await bcrypt.hash(req.locals.profile.password, 10);
    const result = await User.update({ password: password }, { where: { id: req.locals.id } });
    if (result[0] !== 1) {
      throw MESS.USER.ERRORS.INVALID_ID;
    }
    responseHlp.success(req, res, next);
  } catch (err) {
    /* istanbul ignore next */
    responseHlp.error(req, res, next, err);
  }
};

exports.deleteUser = async (req, res, next) => {
  try {
    const result = await User.destroy({ where: { id: req.locals.id } });
    if (result === 0) {
      throw MESS.USER.ERRORS.INVALID_ID;
    }
    responseHlp.success(req, res, next);
  } catch (err) {
    /* istanbul ignore next */
    responseHlp.error(req, res, next, err);
  }
};
