const User = require('../models/').User;
const sqlHlp = require('../helpers/sql');

const responseHlp = require('../helpers/response');
const MESS = require('../config/messages');

exports.fetchUsers = async (req, res, next) => {
  try {
    req.locals.customquery.attributes = sqlHlp.updateSqlAttributes(req.locals, 'User');
    const users = await User.findAll(req.locals.customquery);
    res.locals.links.push({ "rel": "user", "href": req.baseUrl + '/users/{userId}', "method": 'GET' });
    responseHlp.success(req, res, next, users);
  } catch (err) {
    /* istanbul ignore next */
    responseHlp.error(req, res, next, err);
  }
};

exports.fetchUser = async (req, res, next) => {
  try {
    const user = await User.findById(req.locals.id, sqlHlp.updateSqlAttributes(req.locals.user.role, 'User'));
    if (!user) {
      throw MESS.ADMIN.ERRORS.INVALID_USER;
    }
    res.locals.links.push({ "rel": "user_update_profile", "href": req.baseUrl + '/users/{userId}', "method": 'PUT' });
    res.locals.links.push({ "rel": "user_add_admin_role", "href": req.baseUrl + '/users/{userId}/add-admin-role', "method": 'PATCH' });
    res.locals.links.push({ "rel": "user_remove_admin_role", "href": req.baseUrl + '/users/{userId}/remove-admin-role', "method": 'PATCH' });
    res.locals.links.push({ "rel": "user_active_account", "href": req.baseUrl + '/users/{userId}/active', "method": 'PATCH' });
    res.locals.links.push({ "rel": "user_unactive_account", "href": req.baseUrl + '/users/{userId}/unactive', "method": 'PATCH' });
    res.locals.links.push({ "rel": "user_change_password", "href": req.baseUrl + '/users/{userId}/change-password', "method": 'PATCH' });
    res.locals.links.push({ "rel": "user_delete", "href": req.baseUrl + '/users/{userId}', "method": 'DELETE' });
    res.locals.links.push({ "rel": "user_logs", "href": req.baseUrl + '/logs/?filter=user({userId})', "method": 'GET' });
    responseHlp.success(req, res, next, user);
  } catch (err) {
    /* istanbul ignore next */
    responseHlp.error(req, res, next, err);
  }
};

exports.changeRoleUser = async (req, res, next) => {
  try {
    const result = await User.update(req.locals.profile.role, { where: { id: req.locals.id } });
    if (result[0] !== 1) {
      throw MESS.ADMIN.ERRORS.INVALID_USER;
    }
    responseHlp.success(req, res, next);
  } catch (err) {
    /* istanbul ignore next */
    responseHlp.error(req, res, next, err);
  }
};
