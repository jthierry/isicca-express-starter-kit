const config = require('config');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('../models/').User;
const responseHlp = require('../helpers/response');
const MESS = require('../config/messages');

exports.createUser = async (req, res, next) => {
    try { 
      const user = {
        email: req.locals.profile.email,
        firstName: req.locals.profile.firstName,
        lastName: req.locals.profile.lastName,
        role: req.locals.profile.role
      };
      user.password = await bcrypt.hash(req.locals.profile.password, 10);
      result = await User.create(user);
      req.locals.user.id = result.id;
      responseHlp.success(req, res, next);
    } catch (err) {
      if (err.name === 'ValidationError' || err.name === 'SequelizeUniqueConstraintError') {
        err = MESS.USER.ERRORS.EXISTING_EMAIL;
      }
      /* istanbul ignore next */
      responseHlp.error(req, res, next, err);
    }
  };
  
  exports.loginUser = async (req, res, next) => {
    try {
      const fetchedUser = await User.findOne({ where: {email: req.locals.profile.email} });
      if (!fetchedUser) {
        throw MESS.USER.ERRORS.INVALID_CREDDENTIALS;
      }
      if (!fetchedUser.active) {
        throw MESS.USER.ERRORS.ACCOUNT_DESACTIVATED;
      }
      if (!bcrypt.compareSync(req.locals.profile.password, fetchedUser.password)) {
        throw MESS.USER.ERRORS.INVALID_CREDDENTIALS;
      }
      let token;
      token = jwt.sign(
        {
          email: fetchedUser.email,
          id: fetchedUser.id,
          role: fetchedUser.role
        },
        config.user.JWT_KEY,
        { expiresIn: config.user.JWT_EXPIRATIONTIME_HOUR + 'h' }
      );
      result = {
        token: token,
        expiresIn: config.user.JWT_EXPIRATIONTIME_HOUR * 3600,
        profile: fetchedUser
      };
      res.locals.links.push({ "rel": "profile", "href": req.baseUrl + '/' + result.profile.id, "method": 'GET' });
      req.locals.user.id = result.profile.id;
      responseHlp.success(req, res, next, result);
    } catch (err) {
      /* istanbul ignore next */
      responseHlp.error(req, res, next, err);
    }
  };
  